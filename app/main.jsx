import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import App from './app';
import {configureStore} from './store';
import 'semantic-ui-less/semantic.less';
import { fyndSetup } from './containers/util/fynd';

const store = configureStore();

const render = Component => {
  ReactDOM.render(
    <Provider store={store}>
      <Component />
    </Provider>,
    document.getElementById('root'),
  );
};

if (process.env.NODE_ENV === 'production') {
  fyndSetup(render, App);
} else {
  render(App);
}

if (module.hot) {
  module.hot.accept('./app', () => {
    const newApp = require('./app').default;
    render(newApp);
  });
}
