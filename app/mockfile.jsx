import { IUserData } from './models/user';
import { IProfileData } from './models/profile';
import { IJobData, Kind } from './models/job';
import { IRootState } from './reducers/index';
import { MessageStoreState, MessagesStoreState } from './models/message';
import { UserStoreState } from './models/user';

export const mockProfile: IProfileData = {
    name: 'Mikko Mallikas'
};

export const mockMessage: MessageStoreState = {
    sender: mockProfile,
    message: 'Test message 123'
};

export const mockMessages: MessagesStoreState = [
    {
        sender: mockProfile,
        message: 'Hei'
    }
];

export const mockState = {
    chat: mockMessages
};
