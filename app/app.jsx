import * as React from 'react';
import {HashRouter as Router} from 'react-router-dom';
import BaseContainer from './containers/base';
//import video from './assets/videos/emmaneutraali.mp4';
import 'Styles/main.less';

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className="App">
          <BaseContainer />
        </div>
      </Router>
    );
  }
}

export default App;
