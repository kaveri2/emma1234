export const chars = ["Daniel", "Sara", "Jesse", "Hilla", "Alex", "Nora", "Lukas", "Samuli", "Emma", "Taavi", "Tom", "Veera", "Sanic", "Empty"];
export const delay = {
    chatoptions: 1500,
    startresponsetoplayer: 1000,
    sendresponsetoplayer: 1000,
    sendmessage: 500,
    messagelengthmultiplier: 50,
    commentlengthmultiplier: 10,
    newcomment: 2000,
    loadcomment: 1000,
    chaptertransition: 10000
};
export const delayr = {
    chatoptions: 0,
    startresponsetoplayer: 0,
    sendresponsetoplayer: 0,
    sendmessage: 0,
    messagelengthmultiplier: 0,
    commentlengthmultiplier: 0,
    newcomment: 0,
    loadcomment: 0,
    chaptertransition: 1000
};
