import { applyMiddleware, createStore } from "redux";
// Necessary methods for saving and loading
import { save, load } from "redux-localstorage-simple";

/* eslint-disable */
import rootReducer from "../reducers";

const initialState = {};

export function configureStore() {
    const createStoreWithMiddleware = applyMiddleware(
        save() // Saving done here
    )(createStore);

    const store = createStoreWithMiddleware(
        rootReducer,
        load(), // Loading done here
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
    return store;
}
