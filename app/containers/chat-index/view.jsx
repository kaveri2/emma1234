import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Label, List, Icon, Image, Segment
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import "Styles/chat.less";

export class ChatIndexView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };

        this.jumpToTop = this.jumpToTop.bind(this);
    }

    componentDidMount() {
        this.jumpToTop();
    }

    jumpToTop() {
		window.scrollTo(0,0);
//        this.indextop.scrollIntoView({ behavior: "instant" });
    }

    render() {
        const { gamestate } = this.props;
        const { activecharacters } = gamestate;
        return (
            <div className="j-container">
                <div
                    style={{ float: "left", clear: "both" }}
                    ref={(el) => { this.indextop = el; }}
                />
                <Segment basic>
                    <List divided relaxed size="huge" verticalAlign="middle" style={{ maxHeight: "100%" }}>
                        {activecharacters.map((char) => {
                            return (
                                <List.Item as={Link} to={`/chat/${gamestate.characters[char].name}`} key={gamestate.characters[char].name}>
                                    { gamestate.characters[char].unreadcount > 0
                                    && (
                                        <Label circular color="red" size="mini" className="j-label">
                                            {gamestate.characters[char].unreadcount}
                                        </Label>)
                                    }
                                    <Image
                                        avatar
                                        src={gamestate.characters[char].profile.picture}
                                        className="j-chatList-image"
                                    />
                                    <List.Content>
                                        <List.Header>
                                            {gamestate.characters[char].name}
                                        </List.Header>
                                    </List.Content>
                                    {gamestate.characters[char].status ? <span className="j-dot-active"/> : <span className="j-dot"/>}
                                    <List.Content floated="right">
                                        <Icon name="angle right" size="large" />
                                    </List.Content>
                                </List.Item>
                            );
                        })}

                    </List>
                </Segment>
                <Segment basic padded/>
            </div>
        );
    }
}

ChatIndexView.propTypes = {
    gamestate: PropTypes.object.isRequired
};
