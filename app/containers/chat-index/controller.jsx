import * as React from "react";
import * as PropTypes from "prop-types";
import { ChatIndexView } from "./view";
import { maths } from "../util/maths";
import { texts } from "../util/textutils";

export class ChatIndexController extends React.Component {
    render() {
        const { gamestate } = this.props;

        return (
            <ChatIndexView
                gamestate={gamestate}
            />
        );
    }

    componentWillMount() {
        const { gamestate, gamestateActions } = this.props;
        if (gamestate.activecharacters.length === 0) {
            ///console.log("SIMULATION START");
            gamestateActions.newActiveCharacterAction(gamestate.characters.Daniel);
        }
    }
}

ChatIndexController.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired
};
