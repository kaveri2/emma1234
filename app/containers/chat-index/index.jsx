import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { ChatIndexController } from "./controller";
import * as GamestateActions from "../../actions/gamestate";

class ChatIndex extends React.Component {
    render() {
        const {
            gamestate, gamestateActions
        } = this.props;

        return (
            <ChatIndexController
                gamestate={gamestate}
                gamestateActions={gamestateActions}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        gamestate: state.gamestate
    };
}

function mapDispatchToProps(dispatch) {
    return {
        gamestateActions: bindActionCreators(GamestateActions, dispatch)
    };
}

ChatIndex.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(ChatIndex);
