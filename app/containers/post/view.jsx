import * as React from 'react';
import * as PropTypes from 'prop-types';
import {Comment, Divider, Grid, Icon, Image, Segment} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import 'Styles/post.less';
import productionPath from '../../constants/fynd';
import {CommentBarView} from './commentBar';
import {ProfileModalView} from '../newsfeed/profileModal';
import * as Gamestate from '../../reducers/gamestate';

export class PostView extends React.Component {
  render() {
    const {
      chapters,
      gamestate,
      gamestateActions,
      post,
      newsFeedActions,
      likePost,
      sharePost,
      commentPost,
      dontCommentPost,
      optionsView,
      showOptions,
      loadComments,
    } = this.props;
    console.log(gamestate);

    return (
      <div>
        <div className="j-container">
          {/* NAVIGATION ARROW */}
          <Segment
            as={Link}
            to="/newsfeed"
            basic
            compact
            className="j-navArrow">
            <Icon name="arrow circle left" size="big" color="black" />
          </Segment>

          {/* IMAGE - CONDITIONAL RENDERING */}
          {post.img && (
            <Segment basic compact className="j-content">
              <Image src={post.img} rounded size="large" />
            </Segment>
          )}

          {/* VIDEO - CONDITIONAL RENDERING */}
          {post.mov && (
            <Segment basic compact className="j-content">
              <video controls width="100%">
                <source src={Gamestate.feedvideos[post.mov]} type="video/mp4" />
                Selaimesi ei tue HTML-videota.
              </video>
            </Segment>
          )}
          {/* POST MESSAGE / CAPTION */}
          <Segment basic className="j-caption">
            <Comment.Group>
              <Comment>
                <ProfileModalView
                  character={gamestate.characters[post.sender]}
                  name={gamestate.characters[post.sender].name}
                  trigger={
                    <Comment.Avatar
                      src={Gamestate.profileimages[post.sender]}
                      className="j-avatar j-handcursor"
                    />
                  }
                />
                <Comment.Content>
                  <ProfileModalView
                    character={gamestate.characters[post.sender]}
                    name={gamestate.characters[post.sender].name}
                    trigger={
                      <span className="j-profileName j-handcursor">
                        {gamestate.characters[post.sender].name}
                      </span>
                    }
                  />
                  <Comment.Text>{post.message}</Comment.Text>
                </Comment.Content>
              </Comment>
            </Comment.Group>
          </Segment>

          {/* ACTION BUTTON INSIDE DIVIDERS */}
          <Segment basic className="j-actionDivider">
            <Divider fitted />
            <Grid columns="equal" padded textAlign="center">
              <Grid.Row>
                <Grid.Column>
                  <Icon
                    onClick={() => likePost(post.pid)}
                    name="like"
                    size="large"
                    color={post.liked ? 'red' : undefined}
                    className="j-hoverableIcon"
                  />
                  {post.likes}
                </Grid.Column>
                <Grid.Column>
                  <Icon
                    onClick={() => showOptions()}
                    name="comments"
                    size="large"
                    className="j-hoverableIcon"
                  />
                  {post.comments.length}
                </Grid.Column>
                <Grid.Column>
                  <Icon
                    onClick={() => sharePost(post.pid)}
                    name="share alternate"
                    size="large"
                    color={post.sharedbyme ? 'blue' : undefined}
                    className="j-hoverableIcon"
                  />
                  {post.shares}
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Divider fitted />
          </Segment>

          {/* COMMENTS */}
          <Segment basic className="j-comment">
            <Comment.Group>
              {post.comments.map((comment, index) => {
                if (comment.sender && gamestate.characters[comment.sender]) {
                  return (
                    <Comment key={comment.pid}>
                      <ProfileModalView
                        character={gamestate.characters[comment.sender]}
                        name={comment.sender}
                        trigger={
                          <Comment.Avatar
                            src={
                              gamestate.characters[comment.sender].profile
                                .picture
                            }
                            className="j-avatar j-handcursor"
                          />
                        }
                      />
                      <Comment.Content>
                        <ProfileModalView
                          character={gamestate.characters[comment.sender]}
                          name={comment.sender}
                          trigger={
                            <span className="j-profileName j-handcursor">
                              {comment.sender}
                            </span>
                          }
                        />
                        <Comment.Text>
                          {comment.img && (
                            <Image size="medium">
                              {' '}
                              <img src={comment.img} />{' '}
                            </Image>
                          )}
                          {comment.message}
                        </Comment.Text>
                      </Comment.Content>
                    </Comment>
                  );
                } else if (comment.sender) {
                  return (
                    <Comment key={comment.pid}>
                      <ProfileModalView
                        character={gamestate.characters.Empty}
                        name={comment.sender}
                        trigger={
                          <Comment.Avatar
                            src={gamestate.characters.Empty.profile.picture}
                            className="j-avatar j-handcursor"
                          />
                        }
                        className="j-handcursor"
                      />
                      <Comment.Content>
                        <ProfileModalView
                          character={gamestate.characters.Empty}
                          name={comment.sender}
                          trigger={
                            <span className="j-profileName j-handcursor">
                              {comment.sender}
                            </span>
                          }
                        />
                        <Comment.Text>
                          {comment.img && (
                            <Image size="medium">
                              {' '}
                              <img src={comment.img} />{' '}
                            </Image>
                          )}
                          {comment.message}
                        </Comment.Text>
                      </Comment.Content>
                    </Comment>
                  );
                } else {
                  return <div />;
                }
              })}
            </Comment.Group>
          </Segment>
          {/* FOR EXTRA PADDING */}
          <Segment basic padded />
          <Segment basic padded />
          <Segment basic padded />
          <Segment basic padded />
          <Segment basic padded />
        </div>
        {/* COMMENT OPTIONS SELECT BAR */}
        <CommentBarView
          post={post}
          gamestate={gamestate}
          gamestateActions={gamestateActions}
          chapters={chapters}
          newsFeedActions={newsFeedActions}
          optionsView={optionsView}
          showOptions={showOptions}
          loadComments={loadComments}
          commentPost={commentPost}
          dontCommentPost={dontCommentPost}
        />
      </div>
    );
  }
}

PostView.propTypes = {
  post: PropTypes.object.isRequired,
  chapters: PropTypes.object.isRequired,
  gamestateActions: PropTypes.object.isRequired,
  newsFeedActions: PropTypes.object.isRequired,
  gamestate: PropTypes.object.isRequired,
  likePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  commentPost: PropTypes.func.isRequired,
  dontCommentPost: PropTypes.func.isRequired,
  showOptions: PropTypes.func.isRequired,
  loadComments: PropTypes.func.isRequired,
  optionsView: PropTypes.bool.isRequired,
};
