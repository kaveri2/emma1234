import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Button, Divider, Image, Label, Icon, Segment, Grid
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import tom from "Images/profile/Tom.jpg";
import { texts } from "../util/textutils";
import { maths } from "../util/maths";
import * as Game from "../../constants/game";

export class CommentBarView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            //optionsView: false
        };
    }

    render() {
        const {
            post, showOptions, optionsView, dontCommentPost, commentPost
        } = this.props;
        //const { optionsView } = this.state;

        if ((optionsView === true) && post.commentOptions.length > 0) {
            return (
                <Segment size="tiny" attached="bottom" className="j-commentBarActive">
                    <div>
                        { post.commentOptions.map((comment, index) => {
                            if (comment.text.includes("Älä kommentoi")) {
                                return (
                                    <Segment
                                        basic
                                        size="tiny"
                                        className="j-commentOption"
                                        key="no_comment"
                                    >
                                        <Image avatar src={tom}/>
                                        <span><strong>Älä kommentoi mitään</strong></span>
                                        <Button
                                            circular
                                            color="black"
                                            floated="right"
                                            icon="cancel"
                                            onClick={() => { dontCommentPost(comment, post); }}
                                            style={{ marginTop: "-1em" }}
                                        />
                                    </Segment>
                                );
                            } else {
                                return (
                                    <Segment
                                        basic
                                        size="tiny"
                                        className="j-commentOption"
                                        key={comment.text}
                                    >
                                        <div className="j-commentOption-block">
                                            <div>
                                                <Image avatar src={tom}/>
                                                <span><strong>Tom</strong> {comment.text}</span>
                                            </div>
                                            <Button
                                                circular
                                                color="black"
                                                floated="right"
                                                icon="send"
                                                onClick={() => { commentPost(comment, post); }}
                                                style={{ marginTop: "-1em" }}
                                            />
                                        </div>
                                    </Segment>
                                );
                            }
                        })}
                    </div>
                </Segment>
            );
        }
        if (post.commentOptions.length > 0) {
            return (
                <Segment size="tiny" className="j-commentBar j-pulse" onClick={showOptions}>
                    <div>
                        <Image avatar src={tom}/>
                        <span>Kommentoi...</span>
                    </div>
                </Segment>
            );
        }
        return (
            <Segment size="tiny" className="j-commentBar">
                <div>
                    <Image avatar src={tom}/>
                    <span>Ei kommentteja saatavilla</span>
                </div>
            </Segment>
        );
    }
}

CommentBarView.propTypes = {
    post: PropTypes.object.isRequired,
    showOptions: PropTypes.func.isRequired,
    commentPost: PropTypes.func.isRequired,
    dontCommentPost: PropTypes.func.isRequired,
    optionsView: PropTypes.bool.isRequired
};
