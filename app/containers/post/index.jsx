import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { PostController } from "./controller";
import * as GamestateActions from "../../actions/gamestate";
import * as NewsfeedActions from "../../actions/newsfeed";

class Post extends React.Component {
    render() {
        const {
            chapters,
            gamestate,
            gamestateActions,
            newsFeedActions,
            posts,
            chat,
            match,
            history
        } = this.props;

        return (
            <PostController
                chapters={chapters}
                gamestate={gamestate}
                posts={posts}
                chat={chat}
                gamestateActions={gamestateActions}
                newsFeedActions={newsFeedActions}
                match={match}
                history={history}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        chapters: state.chapters,
        gamestate: state.gamestate,
        posts: state.newsfeed,
        chat: state.chat
    };
}

function mapDispatchToProps(dispatch) {
    return {
        gamestateActions: bindActionCreators(GamestateActions, dispatch),
        newsFeedActions: bindActionCreators(NewsfeedActions, dispatch)
    };
}

Post.propTypes = {
    chapters: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    newsFeedActions: PropTypes.object.isRequired,
    chat: PropTypes.object.isRequired,
    posts: PropTypes.array.isRequired,
    gamestate: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Post));
