import * as React from "react";
import * as PropTypes from "prop-types";
import { PostView } from "./view";
import { texts } from "../util/textutils";
import { maths } from "../util/maths";
import * as Game from "../../constants/game";
import { updateReferences } from "../util/updater";

export class PostController extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: null,
            optionsView: false
        };

        this.loadComments = this.loadComments.bind(this);
        this.loadComment = this.loadComment.bind(this);
        this.sharePost = this.sharePost.bind(this);
        this.likePost = this.likePost.bind(this);
        this.commentPost = this.commentPost.bind(this);
        this.dontCommentPost = this.dontCommentPost.bind(this);
        this.executeCodeblock = this.executeCodeblock.bind(this);
        this.showOptions = this.showOptions.bind(this);
    }

    componentWillMount() {
        const { chapters, match } = this.props;
        const pid = match.params.id;
        //const post = chapters.messages[pid - 1];
        const post = texts.getMessage(parseInt(pid, 10), chapters);
        if (post) {
            this.setState({ post }, () => { this.loadComments(true); });
        }
        this.executeCodeblock(post);
		
		window.scrollTo(0,0);
    }

    showOptions() {
        const { optionsView } = this.state;
        this.setState({ optionsView: true });
    }

    sharePost(pid) {
        const { newsFeedActions } = this.props;
        newsFeedActions.sharePostAction(pid);
    }

    likePost(pid) {
        const { newsFeedActions } = this.props;
        newsFeedActions.likePostAction(pid);
    }

    executeCodeblock(message, post) {
        const { gamestate, chapters, gamestateActions } = this.props;
        const codeblocks = message.codearray;
        let sendmessagenow = true;
        let elsesend = false;
        if (!codeblocks || codeblocks.length === 0) return [sendmessagenow, elsesend];
        //no code present, just send message.
        for (let i = 0; i < codeblocks.length; i++) {
            if (codeblocks[i].executed !== true) { //only execute code not executed before
                const personname = codeblocks[i].person;
                const varname = codeblocks[i].variable;
                const tbl = {
                    personname,
                    varname
                };
                if (codeblocks[i].operation === "EQUAL") {
                    tbl.value = codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    ///console.log(`executed code: ${personname}.
                    ///${varname}=${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "INCREMENT") {
                    const person = gamestate.characters[personname];
                    tbl.value = person[varname] || 0;
                    tbl.value += codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    if (varname === "karma") {
                        if (!texts.alreadyKarma(person, gamestate)) {
                            gamestateActions.newKarmaActiveCharacterAction(person);
                        }
                    }
                    ///console.log(`executed code: ${personname}.
                    ///${varname}+${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "CONDITION") {
                    const bool = maths.checkCondition(codeblocks[i], gamestate);
                    if (bool) {
                        /*//console.log(`checked condition
                            ${personname}.${varname}
                            ${codeblocks[i].condition}
                            ${codeblocks[i].value}
                            and it's true`);*/
                        codeblocks[i].executed = true;
                    } else {
                        if (codeblocks[i].elsegoto) {
                            elsesend = texts.getMessage(codeblocks[i].elsegoto, chapters);
                        }
                        sendmessagenow = false;
                        break;
                        // break jotta ei tarkisteta seuraavia conditioita liian aikasin!
                    }
                } else if (codeblocks[i].operation === "GOTO") {
                    ///console.log(`THIS IS STILL UNDERWAY!
                    /// CONTACT DEVELOPER :D ${codeblocks[i].elsegoto}`);
                    //SOSSSSS TODO
                    //this.nextMessage(texts.getMessage(codeblocks[i].elsegoto, chapters));
                    //jos on goto ni sit ei lähetä nykystä viestiä vaan hyppää heti seuraavaan
                    //sendmessagenow = false;
                    break;
                } else if (codeblocks[i].operation === "NEXT") {
                    ///console.log(`THIS IS ALSO UNDERWAY LOL ${codeblocks[i].elsegoto}`);
                }
            }
        }
        return [sendmessagenow, elsesend];
    }
    ///<<set $newsfeed_1_vlog to false>>

    commentPost(comment, post) {
        const { gamestate, newsFeedActions } = this.props;
        if (comment.codearray) {
            this.executeCodeblock(comment, post);
        }
        //empty the comment options
        post.commentOptions = [];

        //remove other comment options from links
        post.comments[post.comments.length - 1].links = [comment];
        comment.sender = "Tom";
        comment.message = comment.text;
        comment.links = [{ pid: comment.pid }]; //pid on ainoa jota tarvitaan linkeissä
        comment.pid = comment.ownpid;
        ///console.log(comment);
        //add Tom's new comment
        post.comments.push(comment);

        newsFeedActions.updatePostAction(post);
        setTimeout(
            this.loadComments,
            Game.delay.newcomment
        );
    }

    dontCommentPost(comment, post) {
        const { gamestate, newsFeedActions } = this.props;
        if (comment.codeblocks) {
            this.executeCodeblock(comment);
        }
        post.commentOptions = [];
        post.comments[post.comments.length - 1].links = [comment];
        newsFeedActions.updatePostAction(post);
        setTimeout(
            this.loadComments,
            Game.delay.newcomment
        );
    }

    loadLoadToFeed(postname) {
        const {
            chapters, gamestate, gamestateActions,
            posts, chat, history
        } = this.props;
        if (postname.includes("newsfeed")) {
            if (!texts.alreadyPosted(postname, posts)) {
                gamestateActions.pushToFeedAction(postname);
            }
        } else if (postname.includes("END ")) {
            setTimeout(
                () => {
                    gamestateActions.startChapterTransitionAction(
                        texts.getMessageByName(postname, chapters)
                    );
                    history.push("/");
                }, Game.delay.chaptertransition
            );
        } else if (!texts.alreadySendedName(postname, chat)) {
            const newchat = texts.getMessageByName(postname, chapters);
            if (newchat) {
                const name = texts.tagname(newchat.tags);
                if (name) {
                    //console.log(`HIP HEI ${name}`);
                    newchat.sender = name;
                    newchat.state = name;
                }
                if (newchat.sender.length > 0) {
                    if (newchat.message.length > 0) {
                        gamestateActions.unreadCountUpAction(newchat.sender);
                        ///console.log(`${newchat.sender} + 1`);
                    }
                    //jos ei viestin senderiobjekti oo
                    //active_charactereissä, laita se sinne.
                    const sender = gamestate.characters[newchat.sender];
                    if (!texts.alreadyActive(sender, gamestate)) {
                        gamestateActions.newActiveCharacterAction(sender);
                        if (!texts.alreadyKarma(sender, gamestate)) {
                            gamestateActions.newKarmaActiveCharacterAction(sender);
                        }
                    }
                }
                gamestateActions.pushToChatAction(postname);
            }
        }
    }

    loadComment(post, comments, commentOptions, first) {
        const {
            chapters, newsFeedActions
        } = this.props;
        if (post.links.length < 2) { //vain yksi linkki eli kommentti ladataan
            const comment = texts.getMessage(post.links[0].pid, chapters);
            ///console.log(`loading comment ${comment.name}`);
            ///console.log(comment);

            const [sendmessage, elsesend] = this.executeCodeblock(comment, post);

            if (sendmessage) {
                if (comment.loadtofeed) { //moved from one line up to here.
                    this.loadLoadToFeed(comment.loadtofeed);
                }
                ///console.log("load comment", comment.name);
                comments.push(comment);
                newsFeedActions.updatePostAction(post);
                if (comment.links) { //recursion
					if (first) {
						this.loadComment(comment, comments, commentOptions, first);
					} else {
						setTimeout(
							() => {
								this.loadComment(comment, comments, commentOptions, first);
							}, Game.delay.loadcomment
						);//*/ //viive, delay
					}
                }
            } else if (elsesend) {
				if (first) {
                        this.loadComment({ links: [elsesend] }, comments, commentOptions, first);
				} else {
					setTimeout(
						() => {
							this.loadComment({ links: [elsesend] }, comments, commentOptions, first);
						}, Game.delay.loadcomment
					);//*/ //viive, delay
				}
            }
        } else { //tomin valinnat
            //commentoptions are from links!
            ///console.log("COMMENT OPTIONS AVAILABLE");
            if (commentOptions.length === 0) {
                for (let i = 0; i < post.links.length; i++) {
                    const element = post.links[i];
                    element.ownpid = post.pid + (i + 1) / 10;
                    commentOptions.push(element);
                }
            }
            newsFeedActions.updatePostAction(post);
        }
    }

    loadComments(first = false) {
        const { post } = this.state;
        const { newsFeedActions } = this.props;
        if (post.comments.length === 0) {
            if (post.links) {
                this.loadComment(post, post.comments, post.commentOptions, first);
            }
        } else if (post.comments[post.comments.length - 1].links) {
            this.loadComment(
                post.comments[post.comments.length - 1],
                post.comments,
                post.commentOptions,
				first
            );
        }
        ///console.log("post", post);
        newsFeedActions.updatePostAction(post);
        ///console.log("COMMENTS LOADED");
    }

    render() {
        const {
            chapters, gamestate, gamestateActions, newsFeedActions
        } = this.props;
        const { post, optionsView } = this.state;

        return (
            <PostView
                post={post}
                chapters={chapters}
                gamestate={gamestate}
                likePost={this.likePost}
                sharePost={this.sharePost}
                commentPost={this.commentPost}
                dontCommentPost={this.dontCommentPost}
                newsFeedActions={newsFeedActions}
                gamestateActions={gamestateActions}
                showOptions={this.showOptions}
                loadComments={this.loadComments}
                optionsView={optionsView}
            />
        );
    }
}

PostController.propTypes = {
    chapters: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    newsFeedActions: PropTypes.object.isRequired,
    posts: PropTypes.array.isRequired,
    chat: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};
