import * as React from 'react';
import * as PropTypes from 'prop-types';
import {Segment} from 'semantic-ui-react';
import {HashRouter as Router, Route} from 'react-router-dom';

import {LoginView} from './login';
import {TitleView} from './title';
import {ProfileView} from './profile';
import {HeaderView} from './header';
import {FooterView} from './footer';
import {CreditsView} from './credits';

import BaseContainer from '.';
import ChatContainer from '../chat';
import ChatIndexContainer from '../chat-index';
import NewsFeedContainer from '../newsfeed';
import PostContainer from '../post';
import TitleContainer from '../title';
import {AnalyticsMethods as analytics} from '../util/fynd';
export class BaseView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const {gamestate, gamestateActions} = this.props;
    const isMainosEntry = gamestate.characters.globals.mainos || false;
    analytics.registerEvent('startButton', isMainosEntry);
    gamestateActions.updateLoginStateAction(true);
  }

  render() {
    const {gamestate, history} = this.props;

    if (gamestate.loggedIn) {
      if (gamestate.titlescreen) {
        return (
          <Router>
            <div>
              <Route exact path="/" component={TitleContainer} />
              <Route exact path="/:option" component={TitleContainer} />
            </div>
          </Router>
        );
      } else {
        return (
          <Router>
            <div>
              <HeaderView />
              <Route
                exact
                path="/emma1234.html"
                component={NewsFeedContainer}
              />
              <Route exact path="/" component={NewsFeedContainer} />
              <Route exact path="/chat/:char" component={ChatContainer} />
              <Route exact path="/index" component={ChatIndexContainer} />
              <Route exact path="/newsfeed" component={NewsFeedContainer} />
              <Route exact path="/post/:id" component={PostContainer} />
              <Route
                exact
                path="/profile"
                component={ProfileView}
                history={history}
              />
              <Route exact path="/credits" component={CreditsView} />
              <FooterView gamestate={gamestate} />
            </div>
          </Router>
        );
      }
    } else {
      return (
        <LoginView
          isMainosEntry={gamestate.characters.globals.mainos}
          handleClick={this.handleClick}
        />
      );
    }
  }
}

BaseView.propTypes = {
  gamestate: PropTypes.object.isRequired,
  gamestateActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};
