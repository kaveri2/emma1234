import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Grid, Header, Image, Segment, Button
} from "semantic-ui-react";
import Tom from "Images/profile/Tom.jpg";

export class ProfileView extends React.Component {
    constructor(props) {
        super(props);

        this.resetPage = this.resetPage.bind(this);
    }

    resetPage() {
        const { history } = this.props;
        window.localStorage.clear();
        window.location.reload();
        history.push("/");
    }

    render() {
        return (
            <Grid>
                <Grid.Row centered columns="2">
                    <Grid.Column>
                        <Image src={Tom} size="medium" circular centered/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row centered columns="2">
                    <Grid.Column textAlign="center">
                        <Header as="h5">Tom</Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row centered columns="2">
                    <Grid.Column className="j-profilefixer" textAlign="center">
                        <span>Oon tämmöne aika tavallinen tyyppi. Saa tutustua
                            <span role="img" aria-label="Nerd Face">🤓</span>
                        </span>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column textAlign="right">
                        <Header as="h5">Seuraajaa 135</Header>
                    </Grid.Column>
                    <Grid.Column textAlign="left">
                        <Header as="h5">Seurataan 87</Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign="center">
                        <Button onClick={this.resetPage} secondary size="massive">Aloita peli alusta</Button>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        );
    }
}

ProfileView.propTypes = {
    history: PropTypes.object.isRequired,
};
