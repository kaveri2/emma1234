import * as React from "react";
import * as PropTypes from "prop-types";
import { Button, Grid, Segment } from "semantic-ui-react";
import { Link } from "react-router-dom";

export const TitleView = ({ nextTitle, prevTitle, titlepassage }) => (
    <div>
        <Segment basic textAlign="center" className="j-loginButton">
            {titlepassage.text}
            <Button onClick={prevTitle} secondary size="massive">PREV</Button>
            <Button onClick={nextTitle} secondary size="massive">NEXT</Button>
        </Segment>
    </div>

);

TitleView.propTypes = {
    nextTitle: PropTypes.func.isRequired,
    prevTitle: PropTypes.func.isRequired,
    titlepassage: PropTypes.object.isRequired
};
