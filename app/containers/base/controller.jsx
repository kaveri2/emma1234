import * as React from "react";
import * as PropTypes from "prop-types";
import { BaseView } from "./view";

export class BaseController extends React.Component {

    render() {
        const { gamestate, gamestateActions, history } = this.props;

        return (
            <BaseView
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                history={history}
            />
        );
    }
}

BaseController.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};
