import * as React from 'react';
import * as PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {BaseController} from './controller';
import * as GamestateActions from '../../actions/gamestate';
import {AnalyticsMethods as analytics} from '../util/fynd';
class Base extends React.Component {
  constructor(props) {
    super(props);
    this.trackRouteChange = this.trackRouteChange.bind(this);
  }

  trackRouteChange(prevProps) {
    if (!this.props.location || !prevProps || !this.props.location.pathname)
      return;
    // send the new path to analytics
    if (this.props.location.pathname !== prevProps.location.pathname) {
      const isMainosEntry = this.props.gamestate.characters.globals.mainos; // boolean. Set on reducers/gamestate.js

      const newPath = this.props.location.pathname.substring(1);
      if (typeof newPath !== 'string' || newPath.length === 0) return;
      analytics.registerEvent(newPath, isMainosEntry);
    }
  }

  componentDidUpdate(prevProps) {
    this.trackRouteChange(prevProps);
  }

  render() {
    const {gamestate, gamestateActions, history} = this.props;

    return (
      <BaseController
        gamestate={gamestate}
        gamestateActions={gamestateActions}
        history={history}
      />
    );
  }
}

function mapStateToProps(state) {
  return {
    gamestate: state.gamestate,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    gamestateActions: bindActionCreators(GamestateActions, dispatch),
  };
}

Base.propTypes = {
  gamestate: PropTypes.object.isRequired,
  gamestateActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Base),
);
