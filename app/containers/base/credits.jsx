import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Grid, Header, Image, Segment, Button, GridColumn
} from "semantic-ui-react";
import Oamk from "Images/EDU.png";
import Edu from "Images/OAMK.png";
import Buutti from "Images/Buutti.png";

export const CreditsView = () => (
    <Segment basic>
        <font size="3">
            <center>
                <strong>Jäitkö jumiin?</strong> <br/> <br/>
				Avaa chatti, jossa on vihreä pallo ja laita viestiä. Tarkista oletko jättänyt kommentoimatta uuteen postaukseen. <br/>
                <br/>
                <strong> Tekijät </strong> <br/>
				<br/>
                <strong>Käsikirjoitus</strong> Annika Veteli <br/>
                <strong>Kuvittaminen</strong> Saara Savusalo <br/>
                <strong>Animointi</strong> Saara Savusalo <br/>
                <strong>Äänet</strong> Annika Veteli <br/>
                <strong>Hahmosuunnittelu</strong> Saara Savusalo <br/>
                <strong>Meemit</strong> Laura Raappana <br/>
                <strong>UI-suunnittelu</strong> Laura Raappana <br/>
                <strong>Graafinen suunnittelu</strong> Laura Raappana <br/>
                <strong>Pelisuunnittelu</strong> Annika Veteli, Laura
                    Raappana ja Saara Savusalo <br/>
                <strong>Ohjelmointi</strong> Perttu Tuovinen,
                    Fayjus Salehin, Peetu Nuottajärvi (Buutti Oy) <br/>
                <strong>Tuottaja</strong> Laura Raappana <br/>
                <strong>Peli-idea</strong> Annika Veteli ja Laura Raappana <br/>
                <br/>
                <strong>Kiitos</strong><br/>
                <br/>
                Blair Stevenson <br/>
                Nina Patrikka <br/>
                Petri Kulju <br/>
                Teemu Palokangas <br/>
                Karoliina Niemelä <br/>
				<br/>
				Yle: <br/>
                Wesa Aapro <br/>
                Ida-Maria Bergman <br/>
                Hanna-Mari Kauhanen <br/>
                Anna-Leena Lappalainen <br/>
				Antti Saarenpää <br/>
				<br/>
                Alkuperäisen Edulab-työtiimin jäsenet: <br/>
                Julia Faltum, Monika Sarach, Katrin Autenrieth, Ville Vähäsarja <br/>
                Kempeleen Kirkonkylän yhtenäiskoulu <br/>
                <Segment basic/>
                <Image src={Oamk} size="tiny" centered/>
                <Segment basic/>
                <Image src={Edu} size="small" centered/>
                <Segment basic/>
                <Image src={Buutti} size="small" centered/>
                <Segment basic padded/>
            </center>
        </font>
    </Segment>
);

CreditsView.propTypes = {
};
