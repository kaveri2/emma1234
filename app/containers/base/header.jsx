import * as React from "react";
import {
    Grid, Icon, Image, Segment, Button
} from "semantic-ui-react";
import logo from "Images/catchfish_logo.png";
import { Link } from "react-router-dom";

export const HeaderView = () => (
    <Segment className="j-header" size="large" inverted basic secondary>
        <Grid>
            <Grid.Row>
                <Grid.Column mobile={8} tablet={13} computer={15} verticalAlign="middle">
                    <Image src={logo} size="medium" className="j-logo"/>
                </Grid.Column>
                <Grid.Column width={8} tablet={3} computer={1} textAlign="right" verticalAlign="middle">
                    <Button circular icon="info" size="mini" as={Link} to="/credits" />
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </Segment>
);
