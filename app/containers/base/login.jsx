import * as React from 'react';
import * as PropTypes from 'prop-types';
import {Button, Grid, Segment} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
export const LoginView = ({handleClick}) => {
  return (
    <div>
      <div className="j-loginBackground" />
      <Segment basic textAlign="center" className="j-loginButton">
        <Button onClick={handleClick} secondary size="massive">
          Aloita peli
        </Button>
      </Segment>
    </div>
  );
};

LoginView.propTypes = {
  handleClick: PropTypes.func.isRequired,
};
