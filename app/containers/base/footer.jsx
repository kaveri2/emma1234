import * as React from "react";
import * as PropTypes from "prop-types";
import { Label, Menu, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

export class FooterView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeItem: "newsfeed"
        };

        this.handleItemClick = this.handleItemClick.bind(this);
    }

    handleItemClick(e, { name }) {
        this.setState({ activeItem: name });
    }

    render() {
        const { activeItem } = this.state;
        const { gamestate } = this.props;
        return (
            <Menu fixed="bottom" inverted size="massive" className="j-footer" icon widths="3">
                <Menu.Item
                    name="newsfeed"
                    as={Link}
                    to="/newsfeed"
                    active={activeItem === "newsfeed"}
                    onClick={this.handleItemClick}
                >
                    <Icon name="home" size="large"/>
                    { gamestate.feednotificationcount + gamestate.loadtofeed.length > 0
                    && (
                        <Label circular color="red" size="mini" className="j-menu-label">
                            {gamestate.feednotificationcount + gamestate.loadtofeed.length}
                        </Label>
                    )
                    }
                </Menu.Item>

                <Menu.Item
                    as={Link}
                    to="/index"
                    name="chat"
                    active={activeItem === "chat"}
                    onClick={this.handleItemClick}
                >
                    <Icon name="chat" size="large"/>
                    { gamestate.chatnotificationcount > 0
                    && (
                        <Label circular color="red" size="mini" className="j-menu-label">
                            {gamestate.chatnotificationcount}
                        </Label>
                    )
                    }
                </Menu.Item>

                <Menu.Item
                    as={Link}
                    to="/profile"
                    name="profile"
                    active={activeItem === "profile"}
                    onClick={this.handleItemClick}
                >
                    <Icon name="user circle" size="large"/>
                </Menu.Item>
            </Menu>
        );
    }
}

FooterView.propTypes = {
    gamestate: PropTypes.object.isRequired,
};
