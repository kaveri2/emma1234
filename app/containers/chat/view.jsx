import * as React from "react";
import ReactDOM from "react-dom";
import * as PropTypes from "prop-types";
import {
    Segment
} from "semantic-ui-react";
import writingDots from "Images/writing_message.gif";
import { MessageView } from "./message";
import { SendBoxView } from "./sendBox";
import { ChatHeaderView } from "./chatHeader";
import * as Game from "../../constants/game";
import "Styles/chat.less";

export class ChatView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            linkIsAvailable: false,
        };

        this.renderSendBox = this.renderSendBox.bind(this);
        this.checkLinks = this.checkLinks.bind(this);
    }

    componentWillReceiveProps() {
        //vaihda viivätytettyyn huoommmaanan TODO
        this.checkLinks();
    }

    componentWillMount() {
        this.checkLinks();
    }

    componentDidMount() {
        const { gamestate, gamestateActions, match } = this.props;
        gamestateActions.unreadCountZeroAction(match.params.char);
        this.jumpToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    checkLinks() {
        const { gamestate, match } = this.props;
        const { currentmessage } = gamestate;
        if (currentmessage[match.params.char].links
            && currentmessage[match.params.char].links[0]) {
            setTimeout(
                () => {
                    this.setState({ linkIsAvailable: true });
                }, Game.delay.chatoptions
            ); //viive, delay
        } else {
            this.setState({ linkIsAvailable: false });
        }
    }

    scrollToBottom() {
        this.messagesEnd.scrollIntoView({ behavior: "smooth" });
    }

    jumpToBottom() {
        this.messagesEnd.scrollIntoView({ behavior: "instant" });
    }

    renderSendBox() {
        const { gamestate, sendPlayerMessage, match } = this.props;

        return (
            <SendBoxView
                gamestate={gamestate}
                match={match}
                sendPlayerMessage={sendPlayerMessage}
            />
        );
    }

    render() {

        const {
            gamestate, match, messages, writing
        } = this.props;
        const {
            linkIsAvailable
        } = this.state;
        const character = gamestate.characters[match.params.char];
        return (
            <div>
                <div className="j-chatbackground"/>
                <ChatHeaderView
                    character={character}
                    currentmsg={gamestate.currentmessage[character.name]}
                />
                <Segment basic padded/>
                <Segment basic className="j-container">
                    {messages.map((message) => {
                        return <MessageView message={message} key={message.pid}/>;
                    })}
                    {writing
                        && (
                            <MessageView message={{
                                message: <img src={writingDots} className="j-writingDots"/>,
                                sender: "none"
                            }}
                            />
                        )
                    }
                    <div
                        style={{ float: "left", clear: "both" }}
                        ref={(el) => { this.messagesEnd = el; }}
                    />
                </Segment>
                <Segment basic padded/>
                <Segment basic/>
                <Segment basic/>
                { linkIsAvailable && this.renderSendBox() }
                { !linkIsAvailable && <Segment basic/> }
            </div>
        );
    }
}

ChatView.propTypes = {
    messages: PropTypes.arrayOf(
        PropTypes.shape({
            message: PropTypes.string.isRequired,
            sender: PropTypes.string.isRequired
        })
    ).isRequired,
    sendPlayerMessage: PropTypes.func.isRequired,
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    writing: PropTypes.bool.isRequired
};
