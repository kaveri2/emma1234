import * as React from "react";
import * as PropTypes from "prop-types";
import { ChatView } from "./view";
import { maths } from "../util/maths";
import { texts } from "../util/textutils";
import { updateReferences } from "../util/updater";
import * as Game from "../../constants/game";

export class ChatController extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            writing: false
        };

        this.nextMessage = this.nextMessage.bind(this);
        this.executeCodeblock = this.executeCodeblock.bind(this);
        this.postponeConditionCheck = this.postponeConditionCheck.bind(this);
        this.checkAllPostponedConditions = this.checkAllPostponedConditions.bind(this);
        this.send = this.send.bind(this);
        this.loadToChat = this.loadToChat.bind(this);
        this.sendPlayerMessage = this.sendPlayerMessage.bind(this);
    }

    componentWillMount() {
        this.loadToChat();
    }

    loadToChat() {
        //load all messages to chat from gamestate.loadtochat array
        const {
            gamestate,
            chapters,
            gamestateActions,
            chat,
        } = this.props;
        if (gamestate.loadtochat.length === 0) {
            return;
        }
        ///console.log("loading new messages to chat.");
        gamestate.loadtochat.forEach((name) => {
            let message = {};
            if (typeof name === "number") {
                ///console.log("NUBMERPR");
                message = texts.getMessage(name, chapters);
            } else {
                message = texts.getMessageByName(name, chapters);
            }
            if (!texts.alreadySended(message.pid, chat)) {
                ///console.log(`loading message ${name}`);
                this.nextMessage(message);
            }
            gamestateActions.popLoadToChatAction(name);
        });
    }

    componentDidUpdate() { //this should be somewhere else.
        this.loadToChat();
        this.checkAllPostponedConditions();
    }

    nextMessage(message) {
        const {
            gamestate,
            gamestateActions,
            match,
            history,
            chapters,
            posts,
            chat
        } = this.props;

        ///console.log("send", message);
        if (message.tags) {
            const name = texts.tagname(message.tags);
            if (name) {
                //console.log(`HIP HEI ${name}`);
                message.sender = name;
                message.state = name;
            }
        }
        ///console.log(message.sender);
        if (message.sender.length > 0 && message.sender !== match.params.char) {
            if (message.message.length > 0) {
                gamestateActions.unreadCountUpAction(message.sender);
                ///console.log(`${message.sender} + 1`);
            }
            //jos ei viestin senderiobjekti oo
            //active_charactereissä, laita se sinne.
            const senderobj = gamestate.characters[message.sender];
            if (!texts.alreadyActive(senderobj, gamestate)) {
                gamestateActions.newActiveCharacterAction(senderobj);
                if (!texts.alreadyKarma(senderobj, gamestate)) {
                    gamestateActions.newKarmaActiveCharacterAction(senderobj);
                }
            }
        }
        //gamestateActions.pushToChatAction(message.loadtofeed);

        const sendmessagenow = this.executeCodeblock(message);

        if (sendmessagenow) {
            if (message.loadtofeed) { //siirretty rivin ylempää tähän.
                if (message.loadtofeed.includes("newsfeed")) {
                    if (!texts.alreadyPosted(message.loadtofeed, posts)) {
                        gamestateActions.pushToFeedAction(message.loadtofeed);
                    }
                } else if (message.loadtofeed.includes("END ")) {
                    setTimeout(
                        () => {
                            gamestateActions.startChapterTransitionAction(
                                texts.getMessageByName(message.loadtofeed, chapters)
                            );
                            history.push("/");
                        }, Game.delay.chaptertransition
                    );
                } else if (!texts.alreadySendedName(message.loadtofeed, chat)) {
                    const newmessage = texts.getMessageByName(message.loadtofeed, chapters);
                    ///console.log(message.loadtofeed);
                    this.nextMessage(newmessage);
                }
            }
            //console.log(`sending ${message.pid} from nextMessage`);
            if (message.sender && (match.url !== history.location.pathname)) {
                //gamestate.characters[message.sender].unreadcount++;
                gamestateActions.unreadCountUpAction(message.sender);
            }
            this.send(message);
        }
    }

    postponeConditionCheck(message) {
        const { gamestate } = this.props;
        //if already contains message, then return
        for (let i = 0; i < gamestate.postponed.length; i++) {
            if (gamestate.postponed[i].pid === message.pid) {
                console.log("ALREADY THERE! SERIOUS ERROR");
                return;
            }
        }
        gamestate.postponed.push(message);
        //test if works
    }

    checkAllPostponedConditions() {
        //still untested
        const { gamestate } = this.props;
        const removals = [];
        for (let i = 0; i < gamestate.postponed.length; i++) {
            const msg = gamestate.postponed[i];
            const executed = this.executeCodeblock(msg);
            if (executed) {
                removals.push(i);
            }
        }

        for (let i = 0; i < removals.length; i++) {
            gamestate.postponed = gamestate.postponed.splice(removals[i], 1);
        }
    }

    executeCodeblock(message) {
        const { gamestate, chapters, gamestateActions } = this.props;
        const codeblocks = message.codearray;
        let sendmessagenow = true;
        if (!codeblocks) return sendmessagenow; //no code present, just send message.
        for (let i = 0; i < codeblocks.length; i++) {
            if (codeblocks[i].executed !== true) { //only execute code not executed before
                const personname = codeblocks[i].person;
                const varname = codeblocks[i].variable;
                const tbl = {
                    personname,
                    varname
                };
                if (codeblocks[i].operation === "EQUAL") {
                    tbl.value = codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    ///console.log(`executed code: ${personname}
                    ///.${varname}=${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "INCREMENT") {
                    const person = gamestate.characters[personname];
                    tbl.value = person[varname] || 0;
                    tbl.value += codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    if (varname === "karma") {
                        if (!texts.alreadyKarma(person, gamestate)) {
                            gamestateActions.newKarmaActiveCharacterAction(person);
                        }
                    }
                    ///console.log(`executed code: ${personname}.
                    ///${varname}+${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "WAIT") {
                    //this.startTimer(codeblocks[i].value);
                    //ttodo
                } else if (codeblocks[i].operation === "CONDITION") {
                    const bool = maths.checkCondition(codeblocks[i], gamestate);
                    if (bool) {
                        /*//console.log(`checked condition
                            ${personname}.${varname}
                            ${codeblocks[i].condition}
                            ${codeblocks[i].value}
                            and it's true`);
                        codeblocks[i].executed = true;*/
                    } else {
                        if (codeblocks[i].elsegoto) {
                            this.nextMessage(texts.getMessage(codeblocks[i].elsegoto, chapters));
                        } else {
                            ///console.log(`postponed condition check from message ${message.pid}`);
                            this.postponeConditionCheck(codeblocks[i], message);
                        }
                        sendmessagenow = false;
                        break;
                        // break jotta ei tarkisteta seuraavia conditioita liian aikasin!
                    }
                } else if (codeblocks[i].operation === "GOTO") {
                    ///console.log(`GO TO MESSAGE ${codeblocks[i].elsegoto}`);
                    this.nextMessage(texts.getMessage(codeblocks[i].elsegoto, chapters));
                    //jos on goto ni sit ei lähetä nykystä viestiä vaan hyppää heti seuraavaan
                    sendmessagenow = false;
                    break;
                } else if (codeblocks[i].operation === "NEXT") {
                    ///console.log(`SHOW ALSO MESSAGE ${codeblocks[i].elsegoto} AFTER DELAY`);
                    const delayedMessage = texts.getMessage(codeblocks[i].elsegoto, chapters);
                    const msglength = delayedMessage.message.length;
                    //PUT WRITING A MESSAGE GIF TO ROLL TODO
                    this.setState({ writing: true });
                    setTimeout(
                        () => {
                            this.setState({ writing: false });
                            this.nextMessage(delayedMessage);
                        }, Game.delay.sendmessage + msglength * Game.delay.messagelengthmultiplier
                    );
                }
            }
        }
        //todo lisää tarkistus että onko tietty viesti saatu
        return sendmessagenow;
    }

    send(messag) {
        const { chatActions, match, gamestateActions } = this.props;
        const message = messag;
        if (!message.state) {
            message.state = match.params.char;
        }
        //set current message:
        gamestateActions.updateGamestateAction({ person: message.state, message });
        if ((message.message && message.message.trim().length > 0) || message.img) {
            //to prevent empty messages from displaying
            chatActions.sendMessageAction(message); //message is the payload
        }

    }

    sendPlayerMessage(text, nextmessagepid, responseIndex) { //responseindex=index of currentlink
        const {
            chapters,
            gamestate,
            match
        } = this.props;
        // Current message pid
        const current = gamestate.currentmessage[match.params.char];
        const choice = current.links[responseIndex];
        const message = {
            sender: "Tom",
            message: text,
            state: match.params.char,
            pid: current.pid + (responseIndex + 1) / 10, //getting the 1.1 format for response Id
        };
        if (choice.img) {
            ///console.log(`sending message with image ${choice.img}`);
            message.img = choice.img;
        }
        this.executeCodeblock(choice); //not checked TODO
        //console.log(`sending ${message.pid} from sendPlayerMessage`);
        ///console.log(message);
        this.send(message);
        //when tom's message is sent, check which response to send or delay
        const delayedMessage = texts.getMessage(nextmessagepid, chapters);
		
        setTimeout(() => {
            this.setState({ writing: delayedMessage.text!=="" });
            setTimeout(
                () => {
                    this.setState({ writing: false });
                    this.nextMessage(delayedMessage);
                }, Game.delay.sendresponsetoplayer
            );
        }, Game.delay.startresponsetoplayer);
    }

    render() {
        const {
            messages, chapters, gamestate, gamestateActions, match
        } = this.props;
        const { writing } = this.state;

        return (
            <ChatView
                messages={messages}
                sendPlayerMessage={this.sendPlayerMessage}
                chapters={chapters}
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                match={match}
                writing={writing}
            />
        );
    }
}

ChatController.propTypes = {
    messages: PropTypes.array.isRequired,
    chat: PropTypes.object.isRequired,
    posts: PropTypes.array.isRequired,
    gamestate: PropTypes.object.isRequired,
    chatActions: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    chapters: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
};
