import * as React from "react";
import * as PropTypes from "prop-types";
import { Message, Segment, Image } from "semantic-ui-react";

export const MessageView = ({ message }) => (
    <Segment basic textAlign={message.sender === "Tom" ? "right" : "left"} className="j-messageviewSegment">
        <Message className={message.sender === "Tom" ? "j-messageviewSender" : "j-messageview"} compact >
            {message.img && (<Image> <img className="j-chat-image" src={message.img}/> </Image>)}
            <Message.Content><strong>{message.message}</strong></Message.Content>
        </Message>
    </Segment>

);

MessageView.propTypes = {
    message: PropTypes.shape({
        sender: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired
    }).isRequired
};
