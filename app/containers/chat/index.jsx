import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { ChatController } from "./controller";
import * as ChatActions from "../../actions/chat";
import * as GamestateActions from "../../actions/gamestate";
import * as NewsfeedActions from "../../actions/newsfeed";

class Chat extends React.Component {
    render() {
        const {
            chapters, chatActions, gamestateActions,
            gamestate, history, match,
            chat, posts, newsFeedActions
        } = this.props;

        return (
            <ChatController
                messages={chat[match.params.char]}
                chat={chat}
                posts={posts}
                chatActions={chatActions}
                chapters={chapters}
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                newsFeedActions={newsFeedActions}
                history={history}
                match={match}
            />
        );
    }
}

function mapStateToProps(state) {
    //console.log(state);
    return {
        chat: state.chat,
        chapters: state.chapters,
        gamestate: state.gamestate,
        posts: state.newsfeed
    };
}

function mapDispatchToProps(dispatch) {
    return {
        chatActions: bindActionCreators(ChatActions, dispatch),
        gamestateActions: bindActionCreators(GamestateActions, dispatch),
        newsFeedActions: bindActionCreators(NewsfeedActions, dispatch)
    };
}

Chat.propTypes = {
    chat: PropTypes.object.isRequired,
    chapters: PropTypes.object.isRequired,
    chatActions: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    newsFeedActions: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    posts: PropTypes.array.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Chat));
