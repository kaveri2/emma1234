import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Grid, Segment, Icon, Image, Label
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import { ProfileModalView } from "../newsfeed/profileModal";

export const ChatHeaderView = ({ character, currentmsg }) => (
    <Segment size="tiny" secondary className="j-chatHeader">
        <Grid>
            <Grid.Row>
                <Grid.Column as={Link} to="/index" mobile={4} tablet={5} computer={6} className="j-chat-backarrow" textAlign="left" verticalAlign="middle">
                    <Icon name="chevron left" size="large" color="black"/>
                </Grid.Column>
                <Grid.Column mobile={8} tablet={6} computer={4} textAlign="center" verticalAlign="middle">
                    <ProfileModalView
                        character={character}
                        name={character.name}
                        trigger={(
                            <div className="j-handcursor">
                                <Image avatar size="mini" spaced="right" src={character.profile.picture} />
                                <span className="j-header-name">
                                    <strong>
                                        {character.name}
                                    </strong>
                                </span>
                                {character.status ? <span className="j-dot-active"/> : <span className="j-dot"/>}
                            </div>
                        )}
                    />
                </Grid.Column>
                <Grid.Column width={4} textAlign="right" verticalAlign="middle">
                    {/* CSS Hack Column */}
                </Grid.Column>
            </Grid.Row>
        </Grid>
    </Segment>
);

ChatHeaderView.propTypes = {
    character: PropTypes.object.isRequired,
    currentmsg: PropTypes.object.isRequired,
};
