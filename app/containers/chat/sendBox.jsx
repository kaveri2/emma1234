import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Button, Grid, Header, Segment, Transition, Image
} from "semantic-ui-react";
import "Styles/chat.less";

export class SendBoxView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            currentLink: 0,
            animate: true
        };

        this.nextLink = this.nextLink.bind(this);
        this.previousLink = this.previousLink.bind(this);
        this.showMessage = this.showMessage.bind(this);
        this.sendMessage = this.sendMessage.bind(this);
    }

    nextLink() {
        const { gamestate, match } = this.props;
        const { currentLink } = this.state;
        const { links } = gamestate.currentmessage[match.params.char];
        if (links[currentLink + 1] !== undefined) {
            this.setState({ currentLink: currentLink + 1 });
        } else {
            this.setState({ currentLink: 0 });
        }
    }

    previousLink() {
        const { gamestate, match } = this.props;
        const { currentLink } = this.state;
        const { links } = gamestate.currentmessage[match.params.char];
        if (links[currentLink - 1] !== undefined) {
            this.setState({ currentLink: currentLink - 1 });
        } else {
            this.setState({ currentLink: links.length - 1 });
        }
    }

    showMessage() {
        const { currentLink, animate } = this.state;
        const { gamestate, match } = this.props;
        const { links } = gamestate.currentmessage[match.params.char];
        if (!links) {
            return (
                <div/>
            );
        }
        if (links.length === 1) {
            return (
                <Grid>
                    <Grid.Column verticalAlign="middle">
                        <Transition animation="browse" duration={500} visible={animate}>
                            <Header as="h5">
                                {links[currentLink].img && <img className="j-sendBoximage" src={links[currentLink].img}/>}
                                {links[currentLink].text}
                            </Header>
                        </Transition>
                    </Grid.Column>
                </Grid>
            );
        } else if (links.length > 1) {
            return (
                <Grid>
                    <Grid.Column width="3" verticalAlign="middle" textAlign="center" className="j-noPadding">
                        <Button circular icon="caret left" secondary onClick={this.previousLink} compact />
                    </Grid.Column>
                    <Grid.Column width="10" verticalAlign="middle">
                        <Transition animation="browse" duration={500} visible={animate}>
                            <Header as="h5">
                                {links[currentLink].img && <img className="j-sendBoximage" src={links[currentLink].img}/>}
                                {links[currentLink].text}
                            </Header>
                        </Transition>
                    </Grid.Column>
                    <Grid.Column width="3" verticalAlign="middle" textAlign="center" className="j-noPadding">
                        <Button circular icon="caret right" secondary onClick={this.nextLink} compact />
                    </Grid.Column>
                </Grid>
            );
        } else {
            return (
                <div/>
            );
        }
    }

    sendMessage() {
        const { currentLink } = this.state;
        const { gamestate, sendPlayerMessage, match } = this.props;
        const { links } = gamestate.currentmessage[match.params.char];
        sendPlayerMessage(links[currentLink].text, links[currentLink].pid, currentLink);
        this.setState({ currentLink: 0 });
    }

    render() {
        const { gamestate, match } = this.props;
        const { links } = gamestate.currentmessage[match.params.char];

        return (
            <div className="j-sendBox-parent">
                <Segment basic className="j-sendBox">
                    <Grid>
                        <Grid.Column width="13">
                            <Segment>
                                {this.showMessage()}
                            </Segment>
                        </Grid.Column>
                        <Grid.Column width="3" verticalAlign="middle" textAlign="center" className="j-noPadding">
                            <Button circular icon="send" size="huge" onClick={this.sendMessage}/>
                            {/* <Icon name="send" size="large" color="grey" inverted circular/> */}
                        </Grid.Column >
                    </Grid>
                </Segment>
            </div>
        );
    }
}

SendBoxView.propTypes = {
    sendPlayerMessage: PropTypes.func.isRequired,
    gamestate: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
};
