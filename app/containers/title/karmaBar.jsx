import * as React from "react";
import * as PropTypes from "prop-types";

export class KarmaBar extends React.Component {
    render() {
        const { karma } = this.props;
        const karmaPercent = Math.floor((50 / 3) * Math.abs(karma));
        const karmaBar = {
            display: "inline-block",
            width: "30vw",
            marginLeft: "2.5vw",
            marginRight: "2.5vw",
            height: "3vh",
            background: "#F3F3F3",
            borderRadius: "0.10%",
            fontSize: "1% !important",
        };
        const fillerPos = {
            position: "relative",
            background: "#66f7bc",
            width: `${karmaPercent}%`,
            height: "100%",
            left: "50%"
        };

        const fillerNeg = {
            position: "relative",
            background: "red",
            width: `${karmaPercent}%`,
            transform: "translateX(-100%)",
            height: "100%",
            left: "50%"
        };

        const fillerNeutral = {
            position: "relative",
            background: "#000000",
            width: "1%",
            height: "100%",
            left: "50%"
        };

        let fillerStyle;
        if (karma === 0) {
            fillerStyle = fillerNeutral;
        } else if (karma > 0) {
            fillerStyle = fillerPos;
        } else {
            fillerStyle = fillerNeg;
        }

        return (
            <span className="j-karmaScale">
                <span role="img" aria-label="pouting face">😡</span>
                <div style={karmaBar}>
                    <div style={fillerStyle}/>
                </div>
                <span role="img" aria-label="smiling face with smiling eyes">😊</span>
            </span>
        );
    }
}

KarmaBar.propTypes = {
    karma: PropTypes.number.isRequired
};
