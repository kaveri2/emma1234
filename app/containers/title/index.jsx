import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { TitleController } from "./controller";
import * as GamestateActions from "../../actions/gamestate";

class Title extends React.Component {
    render() {
        const {
            gamestate, gamestateActions, history, match
        } = this.props;

        return (
            <TitleController
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                history={history}
                match={match}
            />
        );
    }
}

function mapStateToProps(state) {
    //console.log(state);
    return {
        gamestate: state.gamestate,
    };
}

function mapDispatchToProps(dispatch) {
    return {
        gamestateActions: bindActionCreators(GamestateActions, dispatch)
    };
}

Title.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Title));
