import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Button, Header, Icon, Image, Segment, Grid
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import "Styles/chat.less";

import background1 from "Images/boxbackground1.png";
import background2 from "Images/boxbackground2.png";
import background3 from "Images/boxbackground3.png";
import background4 from "Images/boxbackground4.png";
import background5 from "Images/boxbackground5.png";
import background6 from "Images/boxbackground6.png";
import background7 from "Images/boxbackground7.png";

import good from "Images/ending_good.png";
import neutral from "Images/ending_neutral.png";
import bad from "Images/ending_bad.png";

import { KarmaBar } from "./karmaBar";
import { maths } from "../util/maths";

export class TitleView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

        this.disableTitleScreen = this.disableTitleScreen.bind(this);
        this.renderBackground = this.renderBackground.bind(this);
        this.resetPage = this.resetPage.bind(this);
    }

    resetPage() {
        const { history } = this.props;
        window.localStorage.clear();
        window.location.reload();
        history.push("/");
    }

    disableTitleScreen() {
        const { gamestateActions, gamestate, history } = this.props;
        ///console.log("titlepassage", gamestate.titlepassage);
        gamestateActions.disableTitleScreenAction();
        history.push("/");
        console.log(`START CHAPTER ${gamestate.currentchapter}`);
        gamestateActions.pushToFeedAction(gamestate.titlepassage.loadtofeed);
    }

    renderBackground() {

        const backgrounds = [
            background1,
            background2,
            background3,
            background4,
            background5,
            background6,
            background7
        ];

        const { gamestate } = this.props;
        return <Image className="j-boxbackground j-day-image" src={backgrounds[gamestate.currentchapter - 1]}/>;
    }

    render() {
        const { gamestate, match } = this.props;
        const activeCharacters = gamestate.karmaactivecharacters;
        ///console.log(activeCharacters);
        ///console.log(gamestate.characters.Hilla.karma);
        if (match.params.option === "ending") {
            return (
                <div style={{ width: "100" }}>
                    {this.renderBackground()}
                    <Grid>
                        <Grid.Row centered columns="1">
                            <Grid.Column mobile={14} tablet={10} computer={6}>
                                {gamestate.characters.Hilla.karma <= -2 && (
                                    <Image src={bad} size="100%"/>
                                )}
                                {gamestate.characters.Hilla.karma >= 2 && (
                                    <Image src={good} size="100%"/>
                                )}
                                {gamestate.characters.Hilla.karma > -2
                                    && gamestate.characters.Hilla.karma < 2 && (
                                    <Image src={neutral} size="100%"/>
                                )}
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row centered columns="3">
                            <Grid.Column mobile={14} tablet={10} computer={6}>
                                <Image src={require("Images/endings.png")} size="100%"/>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row centered columns="1">
                            <Grid.Column centered textAlign="center">
                                <Button className="j-pelaauudelleen" onClick={this.resetPage} secondary size="massive">PELAA UUDELLEEN</Button>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Image className="j-tiltbox" src={require("Images/green_tiltbox.png")}/>
                </div>
            );
        } else if (match.params.option === "stats") {
            return (
                <div style={{ width: "100" }}>
                    {this.renderBackground()}
                    <Segment basic className="j-stats" textAlign="center">
                        <Header as="h3">Tekemäsi valinnat: </Header>
                        {gamestate.titlepassage.texts.map((code) => {
                            return (
                                <Segment basic className="j-state" key={code}>
                                    <span>
                                        <strong>{code}</strong>
                                    </span>
                                </Segment>
                            );
                        })}
                    </Segment>
                    {gamestate.currentchapter - 1 < 6 && (
                        <span className="j-title">
                            <strong>Aloita seuraava päivä</strong>
                            <Button onClick={this.disableTitleScreen} circular icon="arrow right" size="large" color="black"/>
                        </span>
                    )}
                    {gamestate.currentchapter - 1 === 6 && (
                        <span className="j-title">
                            <strong>Jatka</strong>
                            <Button as={Link} to="/ending" circular icon="arrow right" size="large" color="black"/>
                        </span>
                    )}
                    <Image className="j-tiltbox" src={require("Images/green_tiltbox.png")}/>
                    {/*<Image className="j-unicorn" src={require("Images/crumpy_unicorn.png")}/>*/}
                </div>
            );
        }
        return (
            <div style={{ width: "100" }}>
                {this.renderBackground()}
                <Segment basic className="j-stats" textAlign="center" >
                    <Header as="h3">Tätä mieltä muut ovat sinusta:</Header>
                    {activeCharacters.map((char) => {
                        return (
                            <Segment centered mobile={1} tablet={1} computer={1} basic className="j-state" key={gamestate.characters[char].name}>
                                <Image avatar src={gamestate.characters[char].profile.picture} size="mini"/>
                                <span className="j-karmaname">
                                    <strong>  {gamestate.characters[char].name}</strong>
                                </span>
                                <KarmaBar karma={gamestate.characters[char].karma}/>
                            </Segment>
                        );
                    })}
                    <Segment basic padded/>
                    <Segment basic padded/>
                    <Segment basic padded/>
                    <Segment basic padded/>
                </Segment>
                {gamestate.currentchapter === 1 && (
                    <span className="j-title">
                        <strong>Aloita seuraava päivä</strong>
                        <Button onClick={this.disableTitleScreen} circular icon="arrow right" size="large" color="black"/>
                    </span>
                )
                }
                {gamestate.currentchapter > 1 && (
                    <span className="j-title">
                        <strong>Seuraava</strong>
                        <Button as={Link} to="/stats" circular icon="arrow right" size="large" color="black"/>
                    </span>
                )
                }
                <Image className="j-tiltbox" src={require("Images/green_tiltbox.png")}/>
            </div>
        );
    }
}

TitleView.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
};
