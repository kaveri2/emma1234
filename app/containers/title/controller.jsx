import * as React from "react";
import * as PropTypes from "prop-types";
import { TitleView } from "./view";

export class TitleController extends React.Component {
    render() {
        const {
            gamestate,
            gamestateActions,
            history,
            match
        } = this.props;

        return (
            <TitleView
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                history={history}
                match={match}
            />
        );
    }
}

TitleController.propTypes = {
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired
};
