export const maths = {

    clamp(min, val, max) {
        return Math.min(Math.max(val, min), max);
    },

    compare(post, operator, value) {
        switch (operator) {
        default: return post === value;
        case ">": return post > value;
        case "<": return post < value;
        case ">=": return post >= value;
        case "<=": return post <= value;
        case "==": return post === value;
        case "!=": return post !== value;
        case "===": return post === value;
        case "!==": return post !== value;
        }
    },

    checkCondition(code, gamestate) {
        if (this.compare(
            gamestate.characters[code.person][code.variable], code.condition, code.value
        )) {
            return true;
        } else {
            return false;
        }
    }

};
