export function updateReferences(objectToUpdate, path) {
    const root = Object.assign({}, objectToUpdate);
    let rootTraverse = root;
    for (let i = 0; i < path.length; i++) {
        rootTraverse[path[i]] = Object.assign({}, rootTraverse[path[i]]);
        rootTraverse = rootTraverse[path[i]];
    }

    return root;
}
