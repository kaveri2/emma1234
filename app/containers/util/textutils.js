import * as Game from "../../constants/game";

export const texts = {

    Ucfirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    getMessage(pid, chapters) {
        //getter function, returns a message by its pid value.
        for (let i = 0; i < chapters.messages.length; i++) {
            if (chapters.messages[i].pid === pid) {
                return chapters.messages[i];
            }
        }
        ///console.log(`MESSAGE ${pid} NOT FOUND, FATAL ERROR.`);
        return false;
    },

    getMessageByName(name, chapters) {
        for (let i = 0; i < chapters.messages.length; i++) {
            if (chapters.messages[i].name === name) {
                return chapters.messages[i];
            }
        }
        //console.log(`NO PASSAGE WITH NAME ${name} FOUND! DIES`);
        return false;
    },

    tagname(tags) {
        for (let i = 0; i < tags.length; i++) {
            if (Game.chars.includes(this.Ucfirst(tags[i]))) {
                return this.Ucfirst(tags[i]);
            }
        }
        return false;
    },

    alreadyPosted(name, posts) {
        for (let i = 0; i < posts.length; i += 1) {
            if (posts[i].name === name) {
                return true;
            }
        }
        return false;
    },

    alreadySended(pid, chat) {
        const senders = Object.keys(chat);
        //go through all the chat messages you have gotten
        //from all the fuggen people
        for (let j = 0; j < senders.length; j++) {
            const messages = chat[senders[j]];
            for (let i = 0; i < messages.length; i++) {
                if (messages[i].pid === pid) {
                    return true;
                }
            }
        }
        return false;
    },

    alreadySendedName(name, chat) {
        const senders = Object.keys(chat);
        //go through all the chat messages you have gotten
        //from all the fuggen people
        for (let j = 0; j < senders.length; j++) {
            const messages = chat[senders[j]];
            for (let i = 0; i < messages.length; i++) {
                if (messages[i].name === name) {
                    return true;
                }
            }
        }
        return false;
    },

    alreadyActive(senderobj, gamestate) {
        const chars = gamestate.activecharacters.filter(char => senderobj.name === char);
        if (chars.length > 0) {
            return true;
        }
        return false;
    },

    alreadyKarma(senderobj, gamestate) {
        const chars = gamestate.karmaactivecharacters.filter(char => senderobj.name === char);
        if (chars.length > 0) {
            return true;
        }
        return false;
    },
};
