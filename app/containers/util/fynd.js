const checkAnalytics = () =>
  typeof window.yleAnalytics !== 'undefined' &&
  typeof window.yleAnalytics.trackEvent !== 'undefined'
    ? window.yleAnalytics.trackEvent
    : (a, b) => console.log(`analytiikkaevent ${a} rekisteröity offline`);

const pageName = isMainosEntry =>
  isMainosEntry ? 'Emma1234-mainos-entry' : 'Emma1234';

export const AnalyticsMethods = {
  registerEvent: (name, isMainosEntry) => {
    const trackEvent = checkAnalytics();
    trackEvent(`${pageName(isMainosEntry)}.${name}`, {
      pageName: 'Emma1234',
    });
  },
};

const removeHeaderAndFooter = () => {
  const header = document.querySelector('header');
  if (header) header.remove();
  const footer = document.querySelector('.ydd-footer');
  if (footer) footer.remove();
};

export const fyndSetup = (render, App) => {
  const appName = 'emma123';
  const eventHandlers = {
    onMount: (name, element) => {
      if (name !== appName) {
        console.log(name + ' was mounted, not me.');
        return;
      }
      const root = element.querySelector('#root');
      if (!root) {
        return;
      }

      removeHeaderAndFooter();
      render(App);
    },
  };

  if (window.yleVisualisation) {
    // SYND OR FYND
    window.yleVisualisationEmbeds = window.yleVisualisationEmbeds || {};
    window.yleVisualisationEmbeds[appName] = eventHandlers;
  } else if (!window.yleVisualisation) {
    // ARTICLE RENDERER OR STATIC HOSTING
    eventHandlers.onMount(appName, document.body);
    window.plusApp = window.plusApp || {};
  }
};
