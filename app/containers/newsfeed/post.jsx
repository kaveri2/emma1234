import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Divider, Grid, Icon, Image, Feed, Modal, Header, Segment
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import * as Gamestate from "../../reducers/gamestate";
import { ProfileModalView } from "./profileModal";

export class PostView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        };

        this.navigateToPost = this.navigateToPost.bind(this);
    }

    navigateToPost() {
        const { history, post, gamestate } = this.props;
        if (!post.read) {
            post.read = true;
        }
        history.push(`/post/${Math.floor(post.pid)}`);
    }

    render() {
        const {
            post, index, likePost, sharePost, gamestate
        } = this.props;
        return (
            <Feed.Event>
                <ProfileModalView
                    character={gamestate.characters[post.sender]}
                    name={gamestate.characters[post.sender].name}
                    trigger={(<Feed.Label
                        image={gamestate.characters[post.sender].profile.picture}
                        className="j-newsfeed-profile-img"
                    />
                    )}
                />
                {/*
                <div>
                    {gamestate.characters[post.sender].status ?
                         <span className="j-dot-feed-active"/> : <span className="j-dot-feed"/>}
                </div>
                */}
                <Feed.Content>
                    { post.sharedbyme && (
                        <Feed.Date>
                            Jaoit tämän postauksen aikajanallesi.
                        </Feed.Date>
                    ) }
                    <Feed.Summary>
                        <ProfileModalView
                            character={gamestate.characters[post.sender]}
                            name={gamestate.characters[post.sender].name}
                            trigger={<span className="j-profileName">{gamestate.characters[post.sender].name}</span>}
                        />
                        <Feed.Date></Feed.Date>
                    </Feed.Summary>
                    <Feed.Extra text onClick={this.navigateToPost}>
                        {post.message}
                    </Feed.Extra>
                    {/* IMAGE */}
                    { post.img
                    && (
                        <Feed.Extra images onClick={this.navigateToPost}>
                            <img className="j-newsfeed-img" src={post.img}/>
                        </Feed.Extra>
                    )
                    }
                    {/* IMAGE */}
                    { post.mov
                    && (
                        <Feed.Extra images onClick={this.navigateToPost}>
                            <img className="j-newsfeed-img" src={Gamestate.feedimages[post.mov.trim()]}/>
                        </Feed.Extra>
                    )
                    }
                    <Grid width="100%" columns="equal" textAlign="center" className="j-post">
                        <Grid.Row>
                            <Grid.Column>
                                <Feed.Like onClick={() => likePost(post.pid)}>
                                    <Icon name="like" color={post.liked ? "red" : "grey"}/>
                                    {post.likes}
                                </Feed.Like>
                            </Grid.Column>
                            <Grid.Column>
                                <Feed.Like>
                                    <Icon
                                        name="comment"
                                        className={post.commentOptions.length > 0 ? "j-blinking" : null}
                                        color={post.commentOptions.length > 0 ? undefined : "grey"}
                                        onClick={this.navigateToPost}
                                    />
                                    {post.comments.length}
                                </Feed.Like>
                            </Grid.Column>
                            <Grid.Column>
                                <Feed.Like onClick={() => sharePost(post.pid)}>
                                    <Icon name="share" color={post.sharedbyme ? "blue" : "grey"}/>
                                    {post.shares}
                                </Feed.Like>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Feed.Content>
            </Feed.Event>
        );
    }
}
/*
    <Message className="j-messageview" compact>
        <Message.Content>{Post.message}</Message.Content>
    </Message>
*/

PostView.propTypes = {
    post: PropTypes.shape({
        sender: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired
    }).isRequired,
    likePost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    history: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired
};
