import * as React from "react";
import * as PropTypes from "prop-types";
import { NewsFeedView } from "./view";
import { texts } from "../util/textutils";
import { maths } from "../util/maths";
import { updateReferences } from "../util/updater";

export class NewsFeedController extends React.Component {
    constructor(props) {
        super(props);

        //Remember to bind functions here
        this.likePost = this.likePost.bind(this);
        this.sharePost = this.sharePost.bind(this);
        this.loadToFeed = this.loadToFeed.bind(this);
        this.loadLoadToFeed = this.loadLoadToFeed.bind(this);
        this.commentPost = this.commentPost.bind(this);
        this.alreadyPosted = this.alreadyPosted.bind(this);
    }

    componentWillMount() {
        const { gamestate, newsFeedActions, gamestateActions } = this.props;

        //check if global variables include newsfeed post names
        //and if they are === false, remove them from feed.
        const vars = Object.keys(gamestate.characters.globals);
        for (let j = 0; j < vars.length; j += 1) {
            const variabel = gamestate.characters.globals[vars[j]];
            ///console.log(`global variables: ${vars[j]} is ${variabel} `);
            if (this.alreadyPosted(vars[j]) && variabel === false) {
                newsFeedActions.removePostAction(vars[j]);
                ///console.log(`NYT POISTU POSTAUS ${vars[j]}`);
            }
        }
        gamestateActions.feedUnreadCountZeroAction(gamestate);
		
        this.loadToFeed();
    }

    componentDidUpdate(prevProps) {
        this.loadToFeed();
    }

    executeCodeblock(message) {
        const { gamestate, chapters, gamestateActions } = this.props;
        const codeblocks = message.codearray;
        let sendmessagenow = true;
        if (!codeblocks || codeblocks.length === 0) return sendmessagenow;
        //no code present, just send message.
        for (let i = 0; i < codeblocks.length; i += 1) {
            if (codeblocks[i].executed !== true) { //only execute code not executed before
                const personname = codeblocks[i].person;
                const varname = codeblocks[i].variable;
                const tbl = {
                    personname,
                    varname
                };
                if (codeblocks[i].operation === "EQUAL") {
                    tbl.value = codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    ///console.log(`executed code: ${personname}.
                    ///${varname}=${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "INCREMENT") {
                    const person = gamestate.characters[personname];
                    tbl.value = person[varname] || 0;
                    tbl.value += codeblocks[i].value;
                    gamestateActions.editCharVarAction(tbl);
                    if (varname === "karma") {
                        if (!texts.alreadyKarma(person, gamestate)) {
                            gamestateActions.newKarmaActiveCharacterAction(person);
                        }
                    }
                    ///console.log(`executed code: ${personname}.
                    ///${varname}+${codeblocks[i].value}`);
                    codeblocks[i].executed = true;
                } else if (codeblocks[i].operation === "CONDITION") {
                    const bool = maths.checkCondition(codeblocks[i], gamestate);
                    if (bool) {
                        /*//console.log(`checked condition
                            ${personname}.${varname} ${codeblocks[i].
                                condition} ${codeblocks[i].value}
                            and it's true`);*/
                        codeblocks[i].executed = true;
                    }
                } else if (codeblocks[i].operation === "GOTO" || codeblocks[i].operation === "NEXT") {
                    const goto = texts.getMessage(codeblocks[i].elsegoto, chapters);
                    //console.log(`LOADING ELSEGOTO POST (or Next) ${codeblocks[i].elsegoto}`);
                    ///console.log(`LOADING ELSEGOTO POST (or Next) ${goto.name}`);
                    this.loadLoadToFeed(goto.name);
                    sendmessagenow = false;
                    if (message.name.startsWith("START")) {
                        //PURKKAA :D
                        sendmessagenow = true;
                    }
                    break;
                }
            }
        }
        return sendmessagenow;
    }

    loadLoadToFeed(postname) {
        const {
            chapters, gamestate, gamestateActions,
            posts, chat
        } = this.props;
        if (postname.includes("newsfeed")) {
            if (!texts.alreadyPosted(postname, posts)) {
                gamestateActions.pushToFeedAction(postname);
            }
        } else if (!texts.alreadySendedName(postname, chat)) {
            const newchat = texts.getMessageByName(postname, chapters);
            if (newchat) {
                const name = texts.tagname(newchat.tags);
                if (name) {
                    ///console.log(`TAGNAME ${name}`);
                    newchat.sender = name;
                }
                if (newchat.sender.length > 0) {
                    if (newchat.message.length > 0) {
                        gamestateActions.unreadCountUpAction(newchat.sender);
                        ///console.log(`${newchat.sender} + 1`);
                    }
                    //jos ei viestin senderiobjekti oo
                    //active_charactereissä, laita se sinne.
                    const sender = gamestate.characters[newchat.sender];
                    if (!texts.alreadyActive(sender, gamestate)) {
                        gamestateActions.newActiveCharacterAction(sender);
                        //gamestate.activecharacters = gamestate.activecharacters.concat();
                        if (!texts.alreadyKarma(sender, gamestate)) {
                            gamestateActions.newKarmaActiveCharacterAction(sender);
                        }
                    }
                }
                gamestateActions.pushToChatAction(postname);
            }
        }
    }

    loadToFeed() {
        const {
            chapters, gamestate, gamestateActions, newsFeedActions
        } = this.props;
        if (gamestate.loadtofeed.length === 0) {
            return;
        }
        const newloadtofeed = gamestate.loadtofeed.slice();
        ///console.log("loading to feed", newloadtofeed);
        for (let j = 0; j < newloadtofeed.length; j += 1) {
            const post = texts.getMessageByName(newloadtofeed[j], chapters);
            ///console.log("loading post", post);
            const sendpostnow = this.executeCodeblock(post);
            if (sendpostnow) { //lisätty tarkistus
                if (post.loadtofeed) {
                    this.loadLoadToFeed(post.loadtofeed);
                }
                post.senderObj = gamestate.characters[post.sender];
                if (!post.senderObj) {
                    ///console.log(`sender "${post.sender}" of
                    ///${post.name} not found! NOT SENDING TO FEED~`);
                } else if (!this.alreadyPosted(post.name) && post.likes) {
                    //then it's a real post
                    gamestateActions.feedNotificationCountUpAction();
                    newsFeedActions.newPostAction(post);
                }
            }
            gamestateActions.popLoadToFeedAction(newloadtofeed[j]);
        }
        // //jos on lisätty uusia postauksia luupin aikana ni ajetaan uudelleen
    }

    alreadyPosted(name) {
        const { posts } = this.props;
        for (let i = 0; i < posts.length; i += 1) {
            if (posts[i].name === name) {
                return true;
            }
        }
        return false;
    }

    likePost(pid) {
        const { newsFeedActions } = this.props;
        newsFeedActions.likePostAction(pid);
    }

    sharePost(pid) {
        const { newsFeedActions } = this.props;
        newsFeedActions.sharePostAction(pid);
    }

    commentPost() {
        const { posts } = this.props;
        //
        //default
    }

    render() {
        const {
            posts, chapters, gamestate, history, newsFeedActions
        } = this.props;

        return (
            <NewsFeedView
                posts={posts}
                chapters={chapters}
                gamestate={gamestate}
                newsFeedActions={newsFeedActions}
                likePost={this.likePost}
                sharePost={this.sharePost}
                history={history}
            />
        );
    }
}

NewsFeedController.propTypes = {
    posts: PropTypes.arrayOf(
        PropTypes.shape({
            sender: PropTypes.string.isRequired,
            text: PropTypes.string.isRequired,
            img: PropTypes.string,
            likes: PropTypes.number.isRequired,
            comments: PropTypes.array.isRequired,
            shares: PropTypes.number.isRequired,
        })
    ).isRequired,
    chapters: PropTypes.object.isRequired,
    chat: PropTypes.object.isRequired,
    newsFeedActions: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};
