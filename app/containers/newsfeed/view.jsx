import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Feed, Segment
} from "semantic-ui-react";
import { Redirect } from "react-router-dom";
import { PostView } from "./post";
import "Styles/newsfeed.less";

export class NewsFeedView extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };

        this.jumpToTop = this.jumpToTop.bind(this);
    }

    componentDidMount() {
        this.jumpToTop();
    }

    jumpToTop() {
		window.scrollTo(0,0);
//        this.feedtop.scrollIntoView({ behavior: "instant" });
    }

    render() {
        const {
            posts,
            history,
            likePost,
            sharePost,
            gamestate
        } = this.props;

        return (
            <div className="j-container">
                <div
                    style={{ float: "left", clear: "both" }}
                    ref={(el) => { this.feedtop = el; }}
                />
                <Segment basic>
                    <Feed size="large">
                        {posts.slice(0).reverse().map((post, index) => {
                            return (
                                <PostView
                                    post={post}
                                    index={index}
                                    key={post.pid}
                                    gamestate={gamestate}
                                    likePost={likePost}
                                    sharePost={sharePost}
                                    history={history}
                                />
                            );
                        })}
                    </Feed>
                </Segment>

                {/* FOR EXTRA PADDING */}
                <Segment basic padded />
                <Segment basic padded />
            </div>
        );
    }
}

NewsFeedView.propTypes = {
    posts: PropTypes.arrayOf(
        PropTypes.shape({
            message: PropTypes.string.isRequired,
            sender: PropTypes.string.isRequired
        })
    ).isRequired,
    likePost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired
};
