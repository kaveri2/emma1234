import * as React from "react";
import * as PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { NewsFeedController } from "./controller";
import * as NewsFeedActions from "../../actions/newsfeed";
import * as GamestateActions from "../../actions/gamestate";

class NewsFeed extends React.Component {
    render() {
        const {
            chapters,
            gamestate,
            posts,
            chat,
            gamestateActions,
            newsFeedActions,
            history
        } = this.props;
        return (
            <NewsFeedController
                posts={posts}
                chat={chat}
                newsFeedActions={newsFeedActions}
                chapters={chapters}
                gamestate={gamestate}
                gamestateActions={gamestateActions}
                history={history}
            />
        );
    }
}

function mapStateToProps(state) {
    return {
        chapters: state.chapters,
        gamestate: state.gamestate,
        posts: state.newsfeed,
        chat: state.chat
    };
}

function mapDispatchToProps(dispatch) {
    return {
        newsFeedActions: bindActionCreators(NewsFeedActions, dispatch),
        gamestateActions: bindActionCreators(GamestateActions, dispatch)
    };
}

NewsFeed.propTypes = {
    posts: PropTypes.arrayOf(
        PropTypes.shape({
            sender: PropTypes.string.isRequired,
            message: PropTypes.string.isRequired,
            image: PropTypes.string,
            likes: PropTypes.number.isRequired,
            comments: PropTypes.array.isRequired,
            shares: PropTypes.number.isRequired,
        })
    ).isRequired,
    chapters: PropTypes.object.isRequired,
    chat: PropTypes.object.isRequired,
    newsFeedActions: PropTypes.object.isRequired,
    gamestateActions: PropTypes.object.isRequired,
    gamestate: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NewsFeed));
