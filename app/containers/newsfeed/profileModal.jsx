import * as React from "react";
import * as PropTypes from "prop-types";
import {
    Divider, Grid, Header, Image, Modal, Segment
} from "semantic-ui-react";
import "Styles/profilemodal.less";

export const ProfileModalView = ({ character, name, trigger }) => (
    <Modal closeIcon size="small" trigger={trigger}>
        <Modal.Content>
            {character.profile.picture
                ? <Image circular centered size="tiny" src={character.profile.picture} wrapped/>
                : <Image circular centered size="tiny" src="images/Empty.jpg" wrapped/>
            }
            <Modal.Description className="j-description">
                <Grid textAlign="center">
                    <Grid.Row centered columns="2">
                        <Grid.Column textAlign="center">
                            <Header as="h5">{name}</Header>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row centered columns="2">
                        <Grid.Column textAlign="center">
                            <span>{character.profile.text}</span>
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={2}>
                        <Grid.Column textAlign="right">
                            <Header as="h5">Seuraajaa {character.followers}</Header>
                        </Grid.Column>
                        <Grid.Column textAlign="left">
                            <Header as="h5">Seurataan {character.following}</Header>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Modal.Description>
            <Segment basic textAlign="center" size="tiny">
                {/*<Divider/>
                Tätä mieltä {name} on sinusta: <span role="img"
                aria-label="Beaming Face With Smiling Eyes">😁</span>*/}
            </Segment>
        </Modal.Content>
    </Modal>
);

ProfileModalView.propTypes = {
    character: PropTypes.object.isRequired,
    trigger: PropTypes.object.isRequired,
    name: PropTypes.string.isRequired
};
