import { createAction } from "redux-actions";
import * as Actions from "../constants/actions";

export const sendMessageAction = createAction(Actions.CHAT_SEND);
export const receiveMessagesAction = createAction(Actions.CHAT_RECEIVE);
