import { createAction } from "redux-actions";
import * as Actions from "../constants/actions";

export const updateGamestateAction = createAction(Actions.GAMESTATE_UPDATE);
export const editCharVarAction = createAction(Actions.GAMESTATE_EDITCHARVAR);
export const startChapterTransitionAction = createAction(Actions.GAMESTATE_CHTRANS);
export const disableTitleScreenAction = createAction(Actions.GAMESTATE_DELETETITLE);
export const updateLoginStateAction = createAction(Actions.LOGINSTATE_UPDATE);
export const pushToFeedAction = createAction(Actions.GAMESTATE_FEEDPUSH);
export const pushToChatAction = createAction(Actions.GAMESTATE_CHATPUSH);
export const unreadCountUpAction = createAction(Actions.GAMESTATE_UNREADUP);
export const unreadCountZeroAction = createAction(Actions.GAMESTATE_UNREADZERO);
export const popLoadToChatAction = createAction(Actions.GAMESTATE_LOADTOCHATZERO);
export const popLoadToFeedAction = createAction(Actions.GAMESTATE_LOADTOFEEDZERO);
export const feedUnreadCountZeroAction = createAction(Actions.GAMESTATE_FEEDUNREADZERO);
export const newActiveCharacterAction = createAction(Actions.GAMESTATE_NEWACTIVE);
export const newKarmaActiveCharacterAction = createAction(Actions.GAMESTATE_NEWKARMAACTIVE);
export const feedNotificationCountUpAction = createAction(Actions.GAMESTATE_FEEDUNREADUP);
export const setEnding = createAction(Actions.GAMESTATE_SETENDING);
