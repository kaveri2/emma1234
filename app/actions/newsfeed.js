import { createAction } from "redux-actions";
import * as Actions from "../constants/actions";

export const updateFeedAction = createAction(Actions.FEED_UPDATE);
export const likePostAction = createAction(Actions.FEED_LIKE);
export const sharePostAction = createAction(Actions.FEED_SHARE);
export const openCommentsAction = createAction(Actions.FEED_OPEN);
export const updatePostAction = createAction(Actions.FEED_UPDATEPOST);
export const newPostAction = createAction(Actions.FEED_NEWPOST);
export const removePostAction = createAction(Actions.FEED_REMOVEPOST);
