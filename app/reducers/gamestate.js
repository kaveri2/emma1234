import {handleActions} from 'redux-actions';
import * as Actions from '../constants/actions';
import * as Game from '../constants/game';
import profiles from '../story/profiles.json';

import {updateReferences} from '../containers/util/updater';
import {maths} from '../containers/util/maths';
import {texts} from '../containers/util/textutils';
import productionPath from '../constants/fynd.js';

function importAll(r) {
  const imagearray = r.keys().map(r);
  const images = {};
  imagearray.forEach(img => {
    images[
      img
        .replace('images/', '')
        .replace('videos/', '')
        .replace(productionPath, '')
        .replace('.jpg', '')
        .replace('.mp4', '')
        .replace('.png', '')
        .replace('.gif', '')
    ] = img;
  });
  return images;
  //r.keys().map((item, index) => { images[item.replace("./", "")] = r(item); });
  //return images;
}

export const profileimages = importAll(
  require.context(`Images/profile`, false, /\.(png|jpe?g|svg|gif)$/),
);
export const feedimages = importAll(
  require.context(`Images/feed`, false, /\.(png|jpe?g|svg|gif)$/),
);

export const feedvideos = importAll(
  require.context(`../assets/videos`, false, /\.(mov|mp4)$/),
);

const checkIfContainsMainos = () => {
	const location_url = window.location.href 
	const containsMainos = location_url.indexOf("mainos")
	return containsMainos > -1
}

const initialState = {characters: {}};

for (let i = 0; i < Game.chars.length; i++) {
  initialState.characters[Game.chars[i]] = {
    name: Game.chars[i],
    unreadcount: 0,
    followers: profiles[Game.chars[i]].followers,
    following: profiles[Game.chars[i]].following,
    karma: 0,
    status: false,
    profile: {
      picture: profileimages[Game.chars[i]],
      text: profiles[Game.chars[i]].profile.text,
    },
  };
}
initialState.titlescreen = false;
initialState.titlescreen = false;
initialState.characters.globals = { mainos: checkIfContainsMainos() };

initialState.postponed = []; //messages that contain conditions that weren't yet fulfilled
initialState.chatnotificationcount = 0;
initialState.feednotificationcount = 0;

initialState.loadtofeed = []; //debug
initialState.activecharacters = ['Daniel', 'Jesse']; // Game.chars;
initialState.activecharacters = ['Hilla']; // Game.chars;
initialState.loadtochat = ['daniel_1_7aaaaaaaaa']; //kuvan lähetys
initialState.loadtochat = ['daniel_1_5aaaa']; //kuvan lähetys kaverilta
initialState.loadtochat = ['daniel_1_9_end_2']; //viestiprompti jesselle
initialState.loadtochat = ['daniel_1_9_end']; //jessetesti
initialState.loadtochat = ['hilla_2_2aabbaaa']; //hillatesti
initialState.loadtochat = ['daniel_1_7', 'taavi_2_2']; //jesse_yhteiskuvatesti
initialState.loadtochat = ['samuli_5_2']; //monta henkeä kerralla testi

initialState.activecharacters = []; // Game.chars;
initialState.karmaactivecharacters = []; //Game.chars;

initialState.loadtochat = ['samuli_6_2_end']; //ending
initialState.loadtochat = []; //real start, use this
initialState.loadtofeed = [
  'START 0',
  'newsfeed_0_1',
  'newsfeed_0_2',
  'newsfeed_0_3',
  'newsfeed_0_4',
  'newsfeed_0_0',
]; //REAL START, USE THIS.

initialState.currentchapter = 0;
initialState.currentmessage = {};
for (let i = 0; i < Game.chars.length; i++) {
  initialState.currentmessage[Game.chars[i]] = {
    links: {0: {text: ''}},
  };
}
initialState.loggedIn = false;

/*
//code checkcondition testing
const code = {
    condition: "",
    elsegoto: "",
    executed: false,
    operation: "EQUAL",
    person: "Daniel",
    value: 0,
    variable: "karma"
};

const matti = maths.checkCondition(code, initialState);
console.log(`PELLOLLA MÖKKI, MÖKISSÄ ${matti}`);

*/

export default handleActions(
  {
    [Actions.GAMESTATE_UPDATE]: (state, action) => {
      const gamestate = Object.assign({}, state);
      gamestate.currentmessage[action.payload.person] = action.payload.message;
      return gamestate;
    },

    [Actions.GAMESTATE_EDITCHARVAR]: (state, action) => {
      const gamestate = updateReferences(state, [
        'characters',
        action.payload.personname,
      ]);

      gamestate.characters[action.payload.personname][action.payload.varname] =
        action.payload.value;
      if (action.payload.varname === 'karma') {
        gamestate.characters[action.payload.personname][
          action.payload.varname
        ] = maths.clamp(
          -3,
          gamestate.characters[action.payload.personname][
            action.payload.varname
          ],
          3,
        );
      }
      return gamestate;
    },

    [Actions.GAMESTATE_NEWACTIVE]: (state, action) => {
      const gamestate = Object.assign({}, state);
      if (!texts.alreadyActive(action.payload, state)) {
        gamestate.activecharacters = gamestate.activecharacters.concat(
          action.payload.name,
        );
        ///console.log(`YOU CAN NOW CHAT WITH ${action.payload.name}`);
      }
      return gamestate;
    },

    [Actions.GAMESTATE_NEWKARMAACTIVE]: (state, action) => {
      const gamestate = Object.assign({}, state);
      if (!texts.alreadyKarma(action.payload, state)) {
        gamestate.karmaactivecharacters = gamestate.karmaactivecharacters.concat(
          action.payload.name,
        );
      }
      return gamestate;
    },

    [Actions.LOGINSTATE_UPDATE]: (state, action) => {
      const gamestate = Object.assign({}, state, {
        loggedIn: action.payload,
      });
      return gamestate;
    },
    [Actions.GAMESTATE_UNREADUP]: (state, action) => {
      //const gamestate = Object.assign({}, state);
      const gamestate = updateReferences(state, ['characters', action.payload]);
      gamestate.characters[action.payload].unreadcount++;
      gamestate.chatnotificationcount++;
      return gamestate;
    },

    [Actions.GAMESTATE_UNREADZERO]: (state, action) => {
      const gamestate = updateReferences(state, ['characters', action.payload]);
      const oldcount = gamestate.characters[action.payload].unreadcount;
      gamestate.characters[action.payload].unreadcount = 0;
      gamestate.chatnotificationcount -= oldcount;
      return gamestate;
    },
    [Actions.GAMESTATE_FEEDUNREADUP]: (state, action) => {
      const gamestate = Object.assign({}, state);
      gamestate.feednotificationcount++;
      return gamestate;
    },

    [Actions.GAMESTATE_FEEDUNREADZERO]: (state, action) => {
      const gamestate = Object.assign({}, state);
      gamestate.feednotificationcount = 0;
      return gamestate;
    },

    [Actions.GAMESTATE_DELETETITLE]: (state, action) => {
      const gamestate = Object.assign({}, state);
      delete gamestate.titlepassage;
      gamestate.titlescreen = false;
      return gamestate;
    },

    [Actions.GAMESTATE_CHTRANS]: (state, action) => {
      const gamestate = Object.assign({}, state);
      const passge = action.payload;
      passge.texts = [];
      passge.codearray.forEach(code => {
        const cond = maths.checkCondition(code, gamestate);
        if (cond) {
          passge.texts.push(code.ifgoto.replace(/_/g, ' ').replace(/"/g, ''));
        } else {
          passge.texts.push(code.elsegoto.replace(/_/g, ' ').replace(/"/g, ''));
        }
      });

      gamestate.titlescreen = true;
      gamestate.titlepassage = passge;
      ///console.log(passge);
      gamestate.currentchapter =
        parseInt(passge.name.replace('END ', ''), 10) + 1;
      ///console.log(`GOING TO CHAPTER ${gamestate.currentchapter}`);
      return gamestate;
    },

    [Actions.GAMESTATE_FEEDPUSH]: (state, action) => {
      const gamestate = Object.assign({}, state); //copies the whole state
      if (!gamestate.loadtofeed.includes(action.payload)) {
        gamestate.loadtofeed = gamestate.loadtofeed.concat(action.payload);
        ///console.log(`PUSHED TO FEED: ${action.payload}`);
      }
      return gamestate;
    },
    [Actions.GAMESTATE_CHATPUSH]: (state, action) => {
      const gamestate = Object.assign({}, state); //copies the whole state
      if (!gamestate.loadtochat.includes(action.payload)) {
        gamestate.loadtochat = gamestate.loadtochat.concat(action.payload);
        ///console.log(`PUSHED TO CHAT: ${action.payload}`);
      }
      return gamestate;
    },
    [Actions.GAMESTATE_LOADTOCHATZERO]: (state, action) => {
      const gamestate = Object.assign({}, state); //copies the whole state
      gamestate.loadtochat.splice(
        gamestate.loadtochat.indexOf(action.payload),
        1,
      );
      ///console.log(`removed ${action.payload} from loadtochat`);
      return gamestate;
    },
    [Actions.GAMESTATE_LOADTOFEEDZERO]: (state, action) => {
      const gamestate = Object.assign({}, state); //copies the whole state
      gamestate.loadtofeed.splice(
        gamestate.loadtofeed.indexOf(action.payload),
        1,
      );
      ///console.log(`removed ${action.payload} from loadtofeed`);
      return gamestate;
    },
    [Actions.GAMESTATE_SETENDING]: (state, action) => {
      const gamestate = Object.assign({}, state); //copies the whole state
      gamestate.ending = action.payload;
      ///console.log(`${action.payload} ENDING`);
      return gamestate;
    },
  },
  initialState,
);
