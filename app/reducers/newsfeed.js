import { handleActions } from "redux-actions";
import * as Actions from "../constants/actions";

// THIS IS THE REDUX PART

const initialState = [];
/*format:     {
        sender: "Dan",
        message: "Hi",
        pid: 5
        img: "emma",
        likes: 345,
        comments: [],
        shares: 235,
    }
*/

export default handleActions(
    {
        [Actions.FEED_NEWPOST]: (state, action) => {
            const posts = state.slice();
            posts.push(action.payload);
            return posts;
        },

        [Actions.FEED_REMOVEPOST]: (state, action) => {
            const posts = state.slice();
            return posts.filter(post => post.name !== action.payload);
        },

        [Actions.FEED_UPDATE]: (state, action) => {
            //const messages = state.slice(); //copies the whole state
            //messages.push(action.payload);
            //return messages;
            //never used
        },

        [Actions.FEED_LIKE]: (state, action) => {
            const posts = state.slice(); //copies the whole state
            //pid is the payload
            const post = posts.find(p => p.pid === action.payload);
            if (!post.liked) {
                post.liked = true;
                post.likes++;
            } else {
                post.liked = false;
                post.likes--;
            }
            return posts;
        },

        [Actions.FEED_SHARE]: (state, action) => {
            const posts = state.slice(); //copies the whole state
            const post = posts.find(p => p.pid === action.payload);
            if (!post.sharedbyme) {
                post.sharedbyme = true;
                post.shares++;
            } else {
                post.sharedbyme = false;
                post.shares--;
            }
            return posts;
        },

        [Actions.FEED_UPDATEPOST]: (state, action) => {
            const posts = state.slice(); //copies the whole state
            //post is the payload
            /// console.log(`UPDATING POST ${action.payload.name}`);
            //console.log(action.payload);
            for (let i = 0; i < posts.length; i++) {
                if (posts[i].pid === action.payload.pid) {
                    posts[i] = action.payload;
                    break;
                }
            }
            ///console.log(action.payload);
            //const post = posts[action.payload];
            return posts;
            //never used
        },

        [Actions.FEED_OPEN]: (state, action) => {
            return action.payload;
            //never used
        }
    },
    initialState
);
