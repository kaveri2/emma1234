import { combineReducers } from "redux";
import chat from "./chat";
import newsfeed from "./newsfeed";
import chapters from "./chapters";
import gamestate from "./gamestate";

export default combineReducers({
    chat, chapters, gamestate, newsfeed
});
