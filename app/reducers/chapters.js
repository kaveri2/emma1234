import {handleActions} from 'redux-actions';
import * as Actions from '../constants/actions';
import * as Game from '../constants/game';
import json1 from '../story/chapter_1.json';
import json2 from '../story/chapter_2.json';
import json3 from '../story/chapter_3.json';
import json4 from '../story/chapter_4.json';
import json5 from '../story/chapter_5.json';
import json6 from '../story/chapter_6.json';
import * as Gamestate from './gamestate';
import {texts} from '../containers/util/textutils';

const deepmerge = require('deepmerge');

const initialState = {};
//"state.chapters"

//todo ei vielä löydä sitä vitun json fileä findpassagebyname

function getIndicesOf(searchStr, str) {
  //get all indices of string searchstr that occur in str.
  const searchStrLen = searchStr.length;
  if (searchStrLen === 0) {
    return [];
  }
  let startIndex = 0;
  const indices = [];
  while (str.indexOf(searchStr, startIndex) > -1) {
    indices.push(str.indexOf(searchStr, startIndex));
    startIndex = str.indexOf(searchStr, startIndex) + searchStrLen;
  }
  return indices;
}

function getIndexOfStringAfterIndex(string, preIndex, searchString) {
  return preIndex + string.substring(preIndex).indexOf(searchString);
}

function getIndexOfStringAfter(string, preString, searchString) {
  const preIndex = string.indexOf(preString);
  return getIndexOfStringAfterIndex(string, preIndex, searchString);
}

function removeAllBlocks(text, a, b) {
  //remove blocks that are between strings a and b from text.
  let message = text;
  const blockstarts = getIndicesOf(a, message);
  const blockends = getIndicesOf(b, message);

  const blocks = [];

  for (let j = 0; j < blockstarts.length; j += 1) {
    const block = message.substr(
      blockstarts[j],
      blockends[j] + b.length - blockstarts[j],
    );
    blocks.push(block);
  }
  for (let j = 0; j < blocks.length; j += 1) {
    message = message.replace(blocks[j], ''); //tää ei nyt toimi jos on useampi samanniminen codeblokki
    blocks[j] = blocks[j].substr(
      a.length,
      blocks[j].length - b.length - a.length,
    );
  }

  return [message, blocks];
}

function findPassageByName(name, passages) {
  const jsonp = passages;
  for (let i = 0; i < jsonp.length; i++) {
    if (jsonp[i].name === name) {
      return jsonp[i];
    }
  }
  ///console.log(`NO PASSAGE WITH NAME ${name} FOUND!`);
  //console.log(link);
  return false;
}

function findPassage(pid, passages) {
  const jsonp = passages;
  for (let i = 0; i < jsonp.length; i++) {
    if (jsonp[i].pid === pid) {
      return jsonp[i];
    }
  }
  ///console.log(`NO PASSAGE WITH PID ${pid} FOUND!`);
  //console.log(link);
  return false;
}

function findPassagePidByName(name, passages) {
  const passage = findPassageByName(name, passages);
  if (passage) {
    return passage.pid;
  } else {
    //console.log(`NO PASSAGE (OR PID) WITH NAME ${name} FOUND!`);
    return false;
  }
}

function parseVars(code) {
  if (!(code.substr(0, 1) === '$')) {
    ///console.log(`FATAL ERROR! $ PUUTTUU MUUTTUJASTA ${code}`);
  }

  if (code.includes('_')) {
    const codes = code.substr(1).split('_'); //substr removes first character "$"
    if (Game.chars.includes(texts.Ucfirst(codes[0]))) {
      codes[0] = texts.Ucfirst(codes[0]);
      return codes;
    } else {
      return ['globals', code.substr(1)];
    }
  } else if (Game.chars.includes(texts.Ucfirst(code.substr(1)))) {
    return [texts.Ucfirst(code.substr(1)), 'karma'];
  } else {
    return ['globals', code.substr(1)];
  }
}

function textToCode(text, json) {
  /*
    this function parses twine's SUGARCUBE code (between <<>>)
    supported operations:
    INCREMENT ($matti += 234, $matti -= 234)
    EQUAL (set $matti to 234)
    WAIT (timed 4s)
    GOTO
    NEXT
    CONDITION
    */
  const code = text.split(' ');
  let person = '';
  let variable = '';
  let value = 0;
  let operation = '';
  let condition = '';
  let ifgoto = ''; //vain chapter transitioneissa
  let elsegoto = '';
  if (code[0] === 'set' && code[2] === 'to') {
    //"set $daniel_status to code[3]"
    [person, variable] = parseVars(code[1]);
    const code3 = parseInt(code[3], 10);
    if (code[3] && code[3] === 'true') {
      //jos code[3] on olemassa ja se on true
      value = true;
      //console.log("case true");
      operation = 'EQUAL';
    } else if (code[3] && code[3] === 'false') {
      //jos code[3] on olemassa ja se on flase
      value = false;
      //console.log("case false");
      operation = 'EQUAL';
    } else if (code3 || code3 === 0) {
      //jos code[3] kääntyy intiks eli..
      //jos code3 on numero. on otettava erikseen testi nollalle ku sillä tulis false :D
      value = code3;
      operation = 'EQUAL';
    } else {
      //mut entä jos onki value = $daniel + 1
      operation = 'INCREMENT';
      if (code.length === 6) {
        //case "set $daniel to $daniel + 1" (välilyönneillä)
        if (code[4] === '+') value = parseInt(code[5], 10);
        else if (code[4] === '-') value = -parseInt(code[5], 10);
      } else {
        //case "set $daniel to $daniel+1" EI VÄLILYÖNTEJÄ :0
        /// console.log(`FIX THIS OR WE GOT PROBLEMS: ${text}`);
      }
    }
  } else if (code[1] === '+=') {
    [person, variable] = parseVars(code[0]);
    [value, operation] = [parseInt(code[2], 10), 'INCREMENT'];
  } else if (code[1] === '-=') {
    [person, variable] = parseVars(code[0]);
    [value, operation] = [-parseInt(code[2], 10), 'INCREMENT'];
  } else if (code[0] === 'timed') {
    [person, variable, value, operation] = [
      'globals',
      'time',
      parseInt(code[1].slice(0, -1), 10),
      'WAIT',
    ];
    //EDIT TODO TÄÄ ON VIELÄ KESKEN
  } else if (code[0] === 'next') {
    operation = 'NEXT';
    if (code[1].length === 0) {
      ///console.log(`"${text}" contains empty link [[]]! FIX THIS!`);
    } else {
      const passage = findPassageByName(code[1], json.passages);
      if (!passage) {
        ///console.log(`NOT FOUND PASSAGE "${code[1]}" IN "${text}"!`);
      } else {
        elsegoto = passage.pid;
      }
    }
  } else if (code[0] === 'link') {
    //disregard mutta lissää global muuttuja trueks!!!
  } else if (code[0] === 'if') {
    if (code.length <= 2) {
      operation = '==';
      value = true;
    }

    if (code[3] === 'true' || code[3] === 'false') {
      value = code[3] === 'true';
    } else {
      value = parseInt(code[3], 10);
    }

    [person, variable] = parseVars(code[1]);
    operation = 'CONDITION';

    switch (code[2]) {
      case '<':
        condition = '<';
        break;
      case '!=':
        condition = '!==';
        break;
      case '<=':
        condition = '<=';
        break;
      case '==':
        condition = '===';
        break;
      case '=':
        condition = '===';
        break;
      case '>=':
        condition = '>=';
        break;
      case '>':
        condition = '>';
        break;
      default: ///console.log(`used invalid condition ${code[2]}
      ///, fix it! valid ones are: < <= == > >= != `);
    }
    if (code[4] && code[4] === 'else') {
      const elsecond = code[5].split(':');
      if (elsecond[0] === 'goto') {
        elsegoto = findPassageByName(elsecond[1], json.passages).pid;
      }
    }
    if (code[4] && code[4] === 'got:') {
      ifgoto = code[5];
      elsegoto = code[6];
    }
    /*
        <<goto daniel_1>>
        <<if $olivia > 0 else goto:messagename>>
        <<if $danieljatomriiteli == true else goto:vaihtoehto2>>
        <<if $tomjulkaisikuvan == true else goto:vaihtoehto2>>
        */
    //todo
  } else if (code[0] === 'goto') {
    operation = 'GOTO';
    elsegoto = findPassageByName(code[1], json.passages).pid;
    //elsegoto = findPassageByName(code[1], json.passages).pid; #WANHA
  } else if (code[0] === 'print') {
    //
  } else if (code[0] === '/if') {
    //
  } else {
    ///console.log(`NO CODE TYPE FOUND FOR ${text}`);
  }
  return {
    person,
    variable,
    value,
    operation,
    condition,
    elsegoto,
    ifgoto,
  }; //shorthand object notation
}

function codeblocksToCode(blocks, json) {
  //creates arrays from codeBlocks
  //block = [timed, set asds to asdf, next]
  return blocks.map(block => textToCode(block, json));
}

function feedblocksToFeed(blocks) {
  let img = '';
  let mov = '';
  let likes = 0;
  let shares = 0;
  const sharedbyme = false;
  const liked = false;
  const comments = [];
  for (let j = 0; j < blocks.length; j += 1) {
    const block = blocks[j].split(':');
    switch (block[0].trim()) {
      case 'img':
        if (block[1].trim().length > 0) {
          const imag = Gamestate.feedimages[block[1].trim()];
          if (imag) {
            img = imag;
          } else {
            ///console.log(`IMAGE ${block[1].trim()} NOT FOUND!!!!! SOS`);
          }
        }
        break;
      case 'mov':
        if (block[1].trim().length > 0) {
          mov = block[1].trim();
        }
        break;
      case 'likes':
        likes = parseInt(block[1].trim(), 10);
        break;
      case 'shares':
        shares = parseInt(block[1].trim(), 10);
        break;
      default:
        break;
    }
  }
  return {
    img,
    likes,
    shares,
    comments,
    sharedbyme,
    liked,
    mov,
  };
}

function getTransitionMessage(ifmessage, message) {
  const ifindex = getIndexOfStringAfter(message, '<<if', '>>');
  const elseindex = message.indexOf('<<else>>');
  const iftext = message.slice(ifindex + 2, elseindex); // Odotit Danielia aamulla.
  const else2index = getIndexOfStringAfter(message, 'else>>', '<<');
  const endindex = getIndexOfStringAfter(message, '<<else>>', '<</if>>');
  const elsetext = message.slice(elseindex + '<<else>>'.length, else2index);
  const got = ' got: "';
  const gotot = '" "';
  const tot = '" >>';
  ifmessage =
    ifmessage +
    message.slice(0, ifindex).trim() +
    got +
    iftext.trim().replace(/ /g, '_') +
    gotot +
    elsetext.trim().replace(/ /g, '_') +
    tot;
  message = message.substring(endindex + '<</if>>'.length);
  return [ifmessage, message];
}

function loadJSON(json, chapternumber) {
  console.log(`loading chapter ${chapternumber}...`);
  const loadtofeed = [];

  //reformat json before searching for any code blocks etc.
  for (let i = 0; i < json.passages.length; i++) {
    //pid from string to int.
    json.passages[i].pid =
      parseInt(json.passages[i].pid, 10) + chapternumber * 10000;
  }
  const newpassages = [];
  for (let i = 0; i < json.passages.length; i++) {
    if (json.passages[i].text) {
      if (json.passages[i].name.startsWith('END ')) {
        let transmessage = '';
        let messag = json.passages[i].text;
        while (messag.indexOf('<</if>>') !== -1) {
          [transmessage, messag] = getTransitionMessage(transmessage, messag);
        }
        json.passages[i].text = transmessage + messag;
      } else {
        const message = json.passages[i].text;
        const elseindex = message.indexOf('<<else>>');
        if (elseindex !== -1) {
          const newname = `${json.passages[i].name}_ELSE`;
          const newpid = json.passages[i].pid + 0.01;
          const elsegotostr = ` else goto:${newname}`;

          const ifindex = getIndexOfStringAfter(message, '<<if', '>>');

          const ifmessage =
            message.slice(0, ifindex).trim() +
            elsegotostr +
            message.slice(ifindex, elseindex);
          //nollasta searchindexiin + elsestringi + searchindexistä elseindexiin.
          const tags = json.passages[i].tags || [];
          json.passages[i].text = ifmessage;
          const newpassage = {
            text: message.substring(elseindex + '<<else>>'.length),
            name: newname,
            pid: newpid,
            tags,
          };
          newpassages.push(newpassage);
        }
      }
    }
  }
  for (let i = 0; i < newpassages.length; i++) {
    json.passages.push(newpassages[i]);
  }

  //find codeblocks and other functionality in the message
  for (let i = 0; i < json.passages.length; i++) {
    if (json.passages[i].text) {
      let message = json.passages[i].text;
      let linkblocks = [];
      [message, linkblocks] = removeAllBlocks(message, '[[', ']]');
      delete json.passages[i].links;
      json.passages[i].text = message;

      if (linkblocks.length > 0) {
        const linktable = [];

        if (linkblocks.length === 1) {
          //only one link
          const link = linkblocks[0];
          if (!link.includes('|')) {
            //ei viivaa
            if (
              !(
                json.passages[i].tags &&
                json.passages[i].tags.includes('newsfeed')
              ) &&
              !json.passages[i].name.includes('newsfeed')
            ) {
              //testataan et ei olla newsfeedipostauksessa
              //tai postauksen kommentissa (eli onko nimessä newsfeed)
              //ja jos ei nimeä annettu, mennään suoraan nextiin
              json.passages[i].text += `\n<<next ${link}>>`;
              linkblocks = [];
            }
          }
        }

        for (let j = 0; j < linkblocks.length; j += 1) {
          let link = linkblocks[j];
          let linkname = link;
          const linkobj = {};
          //"tekstiä|linkki"
          //"tekstiä|linkki][$hommat+=1"
          if (link.includes('|')) {
            //linkki mainittu namessa
            [link, linkname] = link.split('|');
          }

          if (linkname.length !== 0 && linkname.indexOf('][') !== -1) {
            // code block linkin lopussa
            let code = '';
            [linkname, code] = linkname.split('][');
            linkobj.codearray = [textToCode(code)];
          }
          const linkPassage = findPassageByName(linkname, json.passages);
          if (linkPassage) {
            linkobj.pid = linkPassage.pid;
          } else {
            ///console.log(`FATAALI ERRORI: link ${linkname}
            /// not found in passage ${json.passages[i].name}`);
          }
          linkobj.link = linkname;

          //if link contains image
          let linkImgBlocks = [];
          [link, linkImgBlocks] = removeAllBlocks(link, '[', ']');
          const things = feedblocksToFeed(linkImgBlocks);

          linkobj.img = things.img;
          linkobj.text = link;
          linktable.push(linkobj);
        }
        json.passages[i].links = linktable;
        if (json.passages[i].links.length === 1) {
          //
        }
      }
    }
    if (json.passages[i].text) {
      let codeblocks = [];
      let commentblocks = [];
      let htmlblocks = [];
      let feedblocks = [];
      let tempmessage = '';

      let message = json.passages[i].text;

      [message, codeblocks] = removeAllBlocks(message, '<<', '>>');
      [tempmessage, htmlblocks] = removeAllBlocks(message, '<', '>');
      if (htmlblocks.length > 0) {
        //<a data-passage=\"newsfeed_1_0\">katso uus postaus</a>
        const indexKatso = getIndexOfStringAfter(
          message,
          '<a data-passage',
          '>',
        );
        const indexPostaus = getIndexOfStringAfter(
          message,
          'a data-passage',
          '<',
        );
        //poista "katso uus postaus"
        message = message.replace(
          message.substring(indexKatso, indexPostaus),
          '',
        );
        //löydä passagen nimi
        const indexA = getIndexOfStringAfter(message, '<a data-passage', '"');
        const indexB = getIndexOfStringAfterIndex(message, indexA + 1, '"');

        const feedpost = message.substring(indexA + 1, indexB);
        json.passages[i].loadtofeed = feedpost; //findPassagePidByName(feedpost);
      }
      [message, htmlblocks] = removeAllBlocks(message, '<', '>');
      //message = tempmessage;
      //a data-passage="newsfeed_0_5"
      /*
            jos löytyy ladatessa jsonia ni laita globals.newsfeed_0_5 boolean falseks
            kun ladataan feediä ni jos newsfeed_0_5 on false ni älä näytä
            mut sit ku tulee viestissä vastaan ni pistetään trueks
            ja lisätään feediin???
            vai helpommin
            */
      [message, commentblocks] = removeAllBlocks(message, '/*', '*/');

      [message, feedblocks] = removeAllBlocks(message, '[', ']');

      if (feedblocks.length > 0) {
        const feedobj = feedblocksToFeed(feedblocks);
        const feedkeys = Object.keys(feedobj);
        for (let j = 0; j < feedkeys.length; j += 1) {
          const key = feedkeys[j];
          json.passages[i][key] = feedobj[key];
        }
      }

      message = message.replace(/\n/g, ''); // remove all newlines

      //find if there's a sender at the start of the string
      const index = message.indexOf(':');
      const sender = texts.Ucfirst(message.substr(0, index).trim());
      message = message.substr(index + 1);

      json.passages[i].sender = sender;
      json.passages[i].state = sender;

      //TODO.
      //atm tää osaa lukea vain sellaset codeblockit jotka ei vaikuta tekstiin
      //esim. <<if asdf>>tee homma <</if>> ei toimi.
      //ja more pressingly <<timed>> <</timed>> ei toimi
      const codearray = codeblocksToCode(codeblocks, json);
      json.passages[i].codearray = codearray;
      //atm codearray is included even when there isnt any code present

      json.passages[i].message = message;
    }
    if (json.passages[i].tags && json.passages[i].tags.includes('newsfeed')) {
      json.passages[i].commentOptions = [];
    }

    delete json.passages[i].position;
  }
  return json;
}

const chapter1 = loadJSON(json1, 1);
const chapter2 = loadJSON(json2, 2);
const chapter3 = loadJSON(json3, 3);
const chapter4 = loadJSON(json4, 4);
const chapter5 = loadJSON(json5, 5);
const chapter6 = loadJSON(json6, 6);

let chapters = deepmerge(chapter1, chapter2);
chapters = deepmerge(chapters, chapter3);
chapters = deepmerge(chapters, chapter4);
chapters = deepmerge(chapters, chapter5);
chapters = deepmerge(chapters, chapter6);

initialState.messages = chapters.passages;

//here messages mean posts and messages

export default handleActions({}, initialState);
