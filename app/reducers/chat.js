import { handleActions } from "redux-actions";
import * as Actions from "../constants/actions";
import * as Game from "../constants/game";

const initialState = {
};

for (let i = 0; i < Game.chars.length; i++) {
    initialState[Game.chars[i]] = [];
}

export default handleActions(
    {
        [Actions.CHAT_SEND]: (state, action) => {
            //message object is the payload
            const chatState = action.payload.state;
            const chat = Object.assign({}, state); //copies the whole chat state
            chat[chatState].push(action.payload);
            return chat;
        },

        [Actions.CHAT_RECEIVE]: (state, action) => {
            return action.payload;
            //never used
        }
    },
    initialState
);
