/* eslint-disable */
const webdriver = require("selenium-webdriver");
const { describe } = require("mocha");

/*

DOCUMENTATION CAN BE FOUND HERE:

https://seleniumhq.github.io/selenium/docs/api/javascript/module/selenium-webdriver/

*/

function importTest(name, path) {
    describe(name, function () {
        require(path);
    });
}

describe("test", function () {
    before(async function() {
        driver = await new webdriver.Builder()
            .withCapabilities( {"browserName" : "firefox" } )
            .build();

        await driver.get('http://localhost:8080');
        await driver.findElement(webdriver.By.className("button")).click();
    });

    importTest("News Feed", './newsFeed');

    after(function() {
        driver.quit();
    });
});