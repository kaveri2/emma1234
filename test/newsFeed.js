/* eslint-disable */
/*const chai = require("chai");
const { describe } = require("mocha");

const expect = chai.expect;
const { Builder, By, until } = require("selenium-webdriver");

it("has 4 posts in start", async () => {
    const profileNames = await driver.findElements(By.className("j-profileName"));
    expect(profileNames.length).to.equal(4);
});

it("displays names correctly", async () => {
    const profileNames = await driver.findElements(By.className("j-profileName"));
    const nameModel = ["Nora", "Daniel", "Nora", "Emma"];
    return Promise.all(profileNames.map(async (profileName, i) => {
        expect(await profileName.getText()).to.equal(nameModel[i]);
    }));
});

it("displays messages correctly", async () => {
    const messages = await driver.findElements(By.className("text extra"));
    const messageModel = [
        "Niin säälittävää porukkaa 🤦🏻‍♀️🤦🏻‍♀️",
        "Kyllä nyt kelpaa taas pelata #lol 🤣",
        "Paartit on parhaat 🥤🔥🔥",
        "Kokonainen yks vuosi yhdessä ❤️😘"
    ];
    return Promise.all(messages.map(async (message, i) => {
        expect(await message.getText()).to.equal(messageModel[i]);
    }));
});

describe("can like posts", () => {
    it("turn like on", async () => {
        const likeIcon = await driver.findElement(By.className("like icon"));
        const likeAmount = await driver.findElement(By.className("like"));
        const oldLikeColor = await likeIcon.getCssValue("color");
        const oldLikeAmount = await likeAmount.getText();
        await likeIcon.click();
        const newLikeColor = await likeIcon.getCssValue("color");
        const newLikeAmount = await likeAmount.getText();
        expect(oldLikeColor).to.equal("rgba(0, 0, 0, 0.5)");
        expect(newLikeColor).to.equal("rgb(219, 40, 40)");
        expect(oldLikeAmount).to.equal("197");
        expect(newLikeAmount).to.equal("198");
    });

    it("like is also shown on post view and is toggleable", async () => {
        const message = await driver.findElement(By.className("text extra"));
        await message.click();
        const likeIcon = await driver.findElement(By.className("like icon"));
        const likeAmount = await driver.findElement(By.xpath('//*[@id="root"]/div/div/div[2]/div[2]/div[2]/div/div[3]'));
        const oldLikeColor = await likeIcon.getCssValue("color");
        const oldLikeAmount = await likeAmount.getText();
        await likeIcon.click();
        const newLikeColor = await likeIcon.getCssValue("color");
        const newLikeAmount = await likeAmount.getText();
        expect(oldLikeColor).to.equal("rgb(219, 40, 40)");
        expect(newLikeColor).to.equal("rgba(0, 0, 0, 0.75)");
        expect(oldLikeAmount).to.equal("198");
        expect(newLikeAmount).to.equal("197");
    });

    it("going back and the state is still saved", async () => {
        const back = await driver.findElement(By.className("black arrow circle left big icon"));
        await back.click();
        const likeIcon = await driver.findElement(By.className("like icon"));
        const likeAmount = await driver.findElement(By.className("like"));
        const likeColor = await likeIcon.getCssValue("color");
        const likeAmountText = await likeAmount.getText();
        expect(likeColor).to.equal("rgba(0, 0, 0, 0.5)");
        expect(likeAmountText).to.equal("197");
    });
});

describe("can share posts", () => {
    it("turn share on", async () => {
        const shareIcon = await driver.findElement(By.className("share icon"));
        const shareAmount = await driver.findElement(By.xpath('//*[@id="root"]/div/div/div[2]/div[1]/div/div[2]/div[2]/div[4]/a[3]'));
        const oldShareColor = await shareIcon.getCssValue("color");
        const oldShareAmount = await shareAmount.getText();
        await shareIcon.click();
        const newshareColor = await shareIcon.getCssValue("color");
        const newshareAmount = await shareAmount.getText();
        expect(oldShareColor).to.equal("rgba(0, 0, 0, 0.5)");
        expect(newshareColor).to.equal("rgb(33, 133, 208)");
        expect(oldShareAmount).to.equal("0");
        expect(newshareAmount).to.equal("1");
    });

    it("share is also shown on post view and is toggleable", async () => {
        const message = await driver.findElement(By.className("text extra"));
        await message.click();
        const shareIcon = await driver.findElement(By.xpath('//*[@id="root"]/div/div/div[2]/div[1]/div/div[2]/div[2]/div[4]/a[3]'));
        const shareAmount = await driver.findElement(By.xpath('//*[@id="root"]/div/div/div[2]/div[2]/div[2]/div/div[3]'));
        const oldShareColor = await shareIcon.getCssValue("color");
        const oldShareAmount = await shareAmount.getText();
        await shareIcon.click();
        const newShareColor = await shareIcon.getCssValue("color");
        const newShareAmount = await shareAmount.getText();
        expect(oldShareColor).to.equal("rgb(219, 40, 40)");
        expect(newShareColor).to.equal("rgba(0, 0, 0, 0.75)");
        expect(oldShareAmount).to.equal("1");
        expect(newShareAmount).to.equal("0");
    });

    it("going back and the state is still saved", async () => {
        const back = await driver.findElement(By.className("black arrow circle left big icon"));
        await back.click();
        const shareIcon = await driver.findElement(By.className("share icon"));
        const shareAmount = await driver.findElement(By.xpath('//*[@id="root"]/div/div/div[2]/div[1]/div/div[2]/div[2]/div[4]/a[3]'));
        const shareColor = await shareIcon.getCssValue("color");
        const shareAmountText = await shareAmount.getText();
        expect(shareColor).to.equal("rgba(0, 0, 0, 0.5)");
        expect(shareAmountText).to.equal("0");
    });
});*/