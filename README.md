Jos haluat muokata pelin nopeutta (viestien ilmestymisen viivettä ym.) niin siihen löytyy muuttujat tiedostosta

`app/constants/game.js`

Siellä olevassa delay-objektissa on viiveet millisekunteina (poislukien multiplier-päätteiset: niillä kerrotaan viestin PITUUTTA - pidemmät viestit tulevat hitaammin!). 
Saman tiedoston delayr-objekti on debuggausta varten oleva versio, jossa viiveet on asetettu nollaksi. Debuggausta varten vaihda delayr-objektin nimi delayksi ja toisinpäin.